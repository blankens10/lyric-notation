
\version "2.22.0"
\include "../lyric-notation.ily"

songLyricsPlain =
    \lyricmode {
    \goodColors
    \rgbVowelRainbow
  Said lil' bitch, you can't fuck with me if you want -- ed to
  These ex -- pen -- sive, these is red bot -- toms, these is bloo -- dy shoes
  Hit the store, I can get 'em both, I don't wan -- na choose
  And I'm quick, cut a nig -- ga off, so don't get comf' -- ta -- ble, look
  I don't dance now, I make mo -- ney moves
  Say I don't got -- ta dance, I make mo - ney move
  If I see you'n I don't speak, that means I don't fuck with you
  I'm a boss, you a wor -- ker, bitch, I make bloo -- dy moves
  }

songSyllablesPlain =
    \lyricmode {
      \partial 4
              \override Slur.direction = #DOWN
              \override Slur.outside-staff-priority = #'()

  r16 s-eh-d16 `l-ih-L8 |
  `b-ih-C8 `y-uw16 k-ae-nt `f-ah-k w-ih-D `m-iy8
  `Q-ih-f16 y-uw @w-aa-n16. n-ih-d32 @t-uw8 `D-iy-z16 Q-eh-k |

  `sp-eh-n s-ih-v32 r32 `D-iy-z16 Q-ih-z `r-eh-d8 b-aa32 `t-ah-mz16.
  `D-iy-z16 Q-ih-z @bl-ah16. d-iy32 @S-uw-z8 `h-ih-t16 D-ah |

  `st-ao-r8 `Q-ay16 k-ae-n `g-eh t-em `b-ow-T8
  `Q-ay32( <>X-ow-n16.) @w-aa16. n-ah32 @C-uw-z8 `Q-ah-nd32 Q-ay-m16. |

  `kw-ih-k8 `k-ah-t32 Q-ah16. `n-ih32 g-ah16. `Q-ao-f16 s-ow
  `d-ow-nt g-eh-t @k-ah-mf16. t-ax32 @`b-el8 `<>l-ah |

  r8 `Q-ay32( <>X-ow-n16.) `d-ae-ns8 `n-aw
  `Q-ay32 m-ey-k16. @m-ah16. n-iy32 @m-uw-vz8 r8 |

  r16 s-ey16 `Q-ay32( <>X-ow-n16.) `g-aa32 t-ah16. `d-ae-ns8
  `Q-ay32 m-ey-k16. @m-ah16. n-iy32 @m-uw-v8 `Q-ih-f16 Q-ay |

  `s-iy y-uw `n-ay( <>X-ow-n) `sp-iy-k8 `D-ae-t16 m-iy-nz
  `Q-ay( <>X-ow-n) @f-ah-k16. <>w-ih-C32 @y-uw8 `Q-ay-m16 Q-ah |

  `b-aa-s8 `y-uw16 Q-ah `w-er k-er `b-ih-C8
  `Q-ay16 m-ey-k @bl-ah16. d-iy32 @m-uw-vz8 r8 | }


songLyricsRKIndexed =
    \lyricmode {
    %{ \goodColors  %}
    %{ \rgbVowelRainbow %}
    \rhColor A yellow
  Said lil' bitch, you can't fuck with me if you want\rh A1 -- ed\rh A2 to\rh A3
  These ex -- pen -- sive, these is red bot -- toms, these is bloo\rh A1 -- dy\rh A2 shoes\rh A3
  Hit the store, I can get 'em both, I don't wan\rh A1 -- na\rh A2 choose\rh A3
  And I'm quick, cut a nig -- ga off, so don't get comf'\rh A1 -- ta\rh A2 -- ble,\rh A3
  look I don't dance now, I make mo\rh A1 -- ney\rh A2 moves\rh A3
  Say I don't got -- ta dance, I make mo\rh A1 -- ney\rh A2 move\rh A3
  If I see you n'I don't speak, that means I don't fuck\rh A1 with\rh A2 you\rh A3
  I'm a boss, you a wor -- ker, bitch, I make bloo\rh A1 -- dy\rh A2 moves\rh A3
  }
songLyricsRK =
    \lyricmode {
    %{ \goodColors  %}
    %{ \rgbVowelRainbow %}
    \rhColor A yellow
  Said lil' bitch, you can't fuck with me if you want\rh A0 -- ed\rh A0 to\rh A0
  These ex -- pen -- sive, these is red bot -- toms, these is bloo\rh A0 -- dy\rh A0 shoes\rh A0
  Hit the store, I can get 'em both, I don't wan\rh A0 -- na\rh A0 choose\rh A0
  And I'm quick, cut a nig -- ga off, so don't get comf'\rh A0 -- ta\rh A0 -- ble,\rh A0
  look I don't dance now, I make mo\rh A0 -- ney\rh A0 moves\rh A0
  Say I don't got -- ta dance, I make mo\rh A0 -- ney\rh A0 move\rh A0
  If I see you n'I don't speak, that means I don't fuck\rh A0 with\rh A0 you\rh A0
  I'm a boss, you a wor -- ker, bitch, I make bloo\rh A0 -- dy\rh A0 moves\rh A0
  }
songSyllablesRK =
    \lyricmode {
        \bracketSettings
        \bracketTextBeforeBreak
        \override Slur.direction = #DOWN
        \override Slur.outside-staff-priority = #'()
        %{ \override HorizontalBracketText.font-size = #-10 %}
        %{ \bracketTextAfterBreak %}
        %{ \genGrooveTupSettings %}
        %{ \grooveTupletTextBeforeBreak %}
        %{ \grooveTupletTextAfterBreak %}
        \partial 4
  r16 s-eh-d16 `l-ih-L8 |
  `b-ih-C8 `y-uw16 k-ae-nt `f-ah-k w-ih-D `m-iy8
  `Q-ih-f16 y-uw \labeledBracket "A" @w-aa-n16. n-ih-d32 \eb @t-uw8 `D-iy-z16 Q-eh-k |

  `sp-eh-n s-ih-v32 r32 `D-iy-z16 Q-ih-z `r-eh-d8 b-aa32 `t-ah-mz16.
  `D-iy-z16 Q-ih-z  @bl-ah16. d-iy32 \eb @S-uw-z8 `h-ih-t16 D-ah |

  `st-ao-r8 `Q-ay16 k-ae-n `g-eh t-em `b-ow-T8
  `Q-ay32( <>X-ow-n16.)  @w-aa16. n-ah32 \eb @C-uw-z8 `Q-ah-nd32 Q-ay-m16. |

  `kw-ih-k8 `k-ah-t32 Q-ah16. `n-ih32 g-ah16. `Q-ao-f16 s-ow
  `d-ow-nt g-eh-t  @k-ah-mf16. t-ax32 \eb @b-el8 `<>l-ah |

  r8 `Q-ay32( <>X-ow-n16.) `d-ae-ns8 `n-aw
  `Q-ay32 m-ey-k16.  @m-ah16. n-iy32 \eb @m-uw-vz8 r8 |

  r16 s-ey16 `Q-ay32( <>X-ow-n16.) `g-aa32 t-ah16. `d-ae-ns8
  `Q-ay32 m-ey-k16.  @m-ah16. n-iy32 \eb @m-uw-v8 `Q-ih-f16 Q-ay |

  `s-iy y-uw `n-ay( <>X-ow-n) `sp-iy-k8 `D-ae-t16 m-iy-nz
  `Q-ay( <>X-ow-n)  @f-ah-k16. <>w-ih-C32 \eb @y-uw8 `Q-ay-m16 Q-ah |

  `b-aa-s8 `y-uw16 Q-ah `w-er k-er `b-ih-C8
  `Q-ay16 m-ey-k  @bl-ah16. d-iy32 \eb @m-uw-vz8 r8 | }

songLyricsMB =
    \lyricmode {
    %{ \goodColors  %}
    %{ \rgbVowelRainbow %}
    \rhColor A yellow
  Said lil' bitch, you can't fuck with me if you want\rh A1 -- ed\rh A2 to\rh A3
  These ex -- pen -- sive, these is red bot -- toms, these is bloo\rh A1 -- dy\rh A2 shoes\rh A3
  Hit the store, I can get 'em both, I don't wan\rh A1 -- na\rh A2 choose\rh A3
  And I'm quick, cut a nig -- ga off, so don't get comf'\rh A1 -- ta\rh A2 -- ble,\rh A3
  look I don't dance now, I make mo\rh A1 -- ney\rh A2 moves\rh A3
  Say I don't got -- ta dance, I make mo\rh A1 -- ney\rh A2 move\rh A3
  If I see you n'I don't speak, that means I don't fuck\rh A1 with\rh A2 you\rh A3
  I'm a boss, you a wor -- ker, bitch, I make bloo\rh A1 -- dy\rh A2 moves\rh A3
}

songSyllablesMB =
    \lyricmode {
        \bracketSettings
        \bracketTextBeforeBreak
        %{ \bracketTextAfterBreak %}
        %{ \genGrooveTupSettings %}
        %{ \grooveTupletTextBeforeBreak %}
        %{ \grooveTupletTextAfterBreak %}
        \partial 4

  r16 s-eh-d16 `l-ih-L8 |
  `b-ih-C8 `y-uw16 k-ae-nt `f-ah-k w-ih-D `m-iy8
  `Q-ih-f16 y-uw \labeledBracket "A" @w-aa-n16. n-ih-d32 \eb @t-uw8 `D-iy-z16 Q-eh-k |

  `sp-eh-n s-ih-v32 r32 `D-iy-z16 Q-ih-z `r-eh-d8 b-aa32 `t-ah-mz16.
  `D-iy-z16 Q-ih-z \labeledBracket "A" @bl-ah16. d-iy32 \eb @S-uw-z8 `h-ih-t16 D-ah |

  `st-ao-r8 `Q-ay16 k-ae-n `g-eh t-em `b-ow-T8
  `Q-ay32( <>X-ow-n16.) \labeledBracket "A" @w-aa16. n-ah32 \eb @C-uw-z8 `Q-ah-nd32 Q-ay-m16. |

  `kw-ih-k8 `k-ah-t32 Q-ah16. `n-ih32 g-ah16. `Q-ao-f16 s-ow
  `d-ow-nt g-eh-t \labeledBracket "A" @k-ah-mf16. t-ax32 \eb @b-el8 `<>l-ah |

  r8 `Q-ay32( <>X-ow-n16.) `d-ae-ns8 `n-aw
  `Q-ay32 m-ey-k16. \labeledBracket "A" @m-ah16. n-iy32 \eb @m-uw-vz8 r8 |

  r16 s-ey16 `Q-ay32( <>X-ow-n16.) `g-aa32 t-ah16. `d-ae-ns8
  `Q-ay32 m-ey-k16. \labeledBracket "A" @m-ah16. n-iy32 \eb @m-uw-v8 `Q-ih-f16 Q-ay |

  `s-iy y-uw `n-ay( <>X-ow-n) `sp-iy-k8 `D-ae-t16 m-iy-nz
  `Q-ay( <>X-ow-n) \labeledBracket "A" @f-ah-k16. <>w-ih-C32 \eb @y-uw8 `Q-ay-m16 Q-ah |

  `b-aa-s8 `y-uw16 Q-ah `w-er k-er `b-ih-C8
  `Q-ay16 m-ey-k \labeledBracket "A" @bl-ah16. d-iy32 \eb @m-uw-vz8 r8 | }

\lyricNotation \songLyricsRK \songSyllablesRK
  \pageBreak
\lyricNotation \songLyricsRK \songSyllablesPlain
  \pageBreak
\oneLineLyricNotation \songLyricsRK \songSyllablesRK
