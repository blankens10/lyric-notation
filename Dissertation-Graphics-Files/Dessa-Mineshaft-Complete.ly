\version "2.22.0"
\include "../lyric-notation.ily"

songLyricsPlain =
  \lyricmode {
  I'm be -- gin -- ning to write like An -- na Ka -- re -- ni -- na, give me a mi -- nute, a mic
  A li -- ttle to like, get rid of the spite, a bit of the pride to fight
  }

songLyricsMBOG =
  \lyricmode {
    %{ \rhColor ax #(x11-color 'Khaki)
    \rhColor a #(x11-color 'Maroon)
    \rhColor b #(x11-color 'Red)
    \rhColor c #(x11-color 'Orange)
    \rhColor d #(x11-color 'Yellow)
    \rhColor e #(x11-color 'Olive)
    \rhColor f #(x11-color 'Green)
    \rhColor g #(x11-color 'Lime)
    \rhColor h #(x11-color 'Cyan)
    \rhColor i #(x11-color 'Teal)
    \rhColor j #(x11-color 'DodgerBlue)
    \rhColor k #(x11-color 'RoyalBlue)
    \rhColor l #(x11-color 'Blue)
    \rhColor m #(x11-color 'Purple)
    \rhColor n #(x11-color 'Fuchsia)

    \rhColor AX #(x11-color 'Khaki)
    \rhColor A #(x11-color 'Maroon)
    \rhColor B #(x11-color 'Red)
    \rhColor C #(x11-color 'Orange)
    \rhColor D #(x11-color 'Yellow)
    \rhColor E #(x11-color 'Olive)
    \rhColor F #(x11-color 'Green)
    \rhColor G #(x11-color 'Lime)
    \rhColor H #(x11-color 'Cyan)
    \rhColor I #(x11-color 'Teal)
    \rhColor J #(x11-color 'DodgerBlue)
    \rhColor K #(x11-color 'RoyalBlue)
    \rhColor L #(x11-color 'Blue)
    \rhColor M #(x11-color 'Purple)
    \rhColor N #(x11-color 'Fuchsia)  %}
      \goodColors
  I'm\rh c2 be\rh b2 --
  gin\rh b2 -- ning\rh b2 to\rh d1 write\rh d2
  like\rh c2 An\rh b1 -- na\rh b1 Ka\rh b1 --
  re\rh b2 -- ni\rh b2 -- na,\rh B1 give\rh B2 me\rh B2 a\rh b1
  mi\rh b2 -- nute,\rh b3 a\rh c1 mic\rh c2 A\rh b1 |
  li\rh b2 -- ttle\rh b3 to\rh d1 like,\rh d2 get\rh b2
  rid\rh b2 of\rh b3 the\rh c1 spite,\rh c2 a\rh b1
  bit\rh b2 of\rh b3 the\rh c1 pride\rh c2 to\rh d1
  fight\rh d2 |
  }

  songLyricsRainbowTest =
    \lyricmode {
        \rgbRainbowGradients

    I'm\rh AY0 be\rh IY0 -- |
    gin\rh ih0 -- ning\rh IH0 to\rh UW0 write\rh ay0
    like\rh AY0 An\rh AE0 -- na\rh Ax0 Ka\rh AX0 --
    re\rh EH0 -- ni\rh IH0 -- na,\rh AX0 give\rh IH0 me\rh IY0 a\rh AX0
    mi\rh IH0 -- nute,\rh Ax0 a\rh AX0 mic\rh AY0 A\rh AX0 |
    li\rh IH0 -- ttle\rh EL0 to\rh UW0 like,\rh AY0 get\rh EH0
    rid\rh IH0 of\rh Ax0 the\rh AX0 spite,\rh AY0 a\rh AX0
    bit\rh IH0 of\rh Ax0 the\rh AX0 pride\rh AY0 to\rh UW0
    fight\rh AY0 |
  }

songLyricsRainbow =
  \lyricmode {
      \rgbRainbowGradients

  I'm\rh AY0 be\rh IY0 --
  gin\rh ih0 -- ning\rh IH0 to\rh UW0 write\rh ay0
  like\rh AY0 An\rh AE0 -- na\rh ax0 Ka\rh AX0 --
  re\rh EH0 -- ni\rh IH0 -- na,\rh AX0 give\rh IH0 me\rh IY0 a\rh AX0
  mi\rh IH0 -- nute,\rh ax0 a\rh AX0 mic\rh AY0 A\rh AX0 |
  li\rh IH0 -- ttle\rh EL0 to\rh UW0 like,\rh AY0 get\rh EH0
  rid\rh IH0 of\rh ax0 the\rh AX0 spite,\rh AY0 a\rh AX0
  bit\rh IH0 of\rh ax0 the\rh AX0 pride\rh AY0 to\rh UW0
  fight\rh AY0 |
  }

songLyricsRainbowAlt =
  \lyricmode {
      \rgbRainbowGradientsAlt

  I'm\rh AY0 be\rh IY0 --
  gin\rh ih0 -- ning\rh IH0 to\rh UW0 write\rh ay0
  like\rh AY0 An\rh AE0 -- na\rh ax0 Ka\rh AX0 --
  re\rh EH0 -- ni\rh IH0 -- na,\rh AX0 give\rh IH0 me\rh IY0 a\rh AX0
  mi\rh IH0 -- nute,\rh ax0 a\rh AX0 mic\rh AY0 A\rh AX0 |
  li\rh IH0 -- ttle\rh EL0 to\rh UW0 like,\rh AY0 get\rh EH0
  rid\rh IH0 of\rh ax0 the\rh AX0 spite,\rh AY0 a\rh AX0
  bit\rh IH0 of\rh ax0 the\rh AX0 pride\rh AY0 to\rh UW0
  fight\rh AY0 |
  }
songLyricsRainbowAltTwo =
  \lyricmode {
      \rgbRainbowGradientsAltTwo

  I'm\rh AY0 be\rh IY0 --
  gin\rh ih0 -- ning\rh IH0 to\rh UW0 write\rh ay0
  like\rh AY0 An\rh AE0 -- na\rh ax0 Ka\rh AX0 --
  re\rh EH0 -- ni\rh IH0 -- na,\rh AX0 give\rh IH0 me\rh IY0 a\rh AX0
  mi\rh IH0 -- nute,\rh ax0 a\rh AX0 mic\rh AY0 A\rh AX0 |
  li\rh IH0 -- ttle\rh EL0 to\rh UW0 like,\rh AY0 get\rh EH0
  rid\rh IH0 of\rh ax0 the\rh AX0 spite,\rh AY0 a\rh AX0
  bit\rh IH0 of\rh ax0 the\rh AX0 pride\rh AY0 to\rh UW0
  fight\rh AY0 |
  }


songSyls =
  \lyricmode {
      \bracketSettings
        %{ \override TupletBracket.outside-staff-priority = #1100 %}
        %{ \override TupletBracket.padding = #0.2 %}
        %{ \override TupletBracket.staff-padding = #3.5 %}
        %{ \override TupletBracket.outside-staff-padding = #0.2 %}
        %{ \override TupletBracket.avoid-scripts = ##f %}
        %{ \override TupletNumber.whiteout = ##t %}
        %{ \override TupletNumber.whiteout-style = #'rounded-box %}

    \partial 8
  \tuplet 3/2 8 { \bB `Q-ay-m8 b-iy16 } |
  \tuplet 3/2 8 { `g-ih n-ih-G16 t-uw } \eb `r-ay-t8
  `l-ay-k \tuplet 3/2 8 {
  `Q-ae16 n-ah
    %{ \tweak HorizontalBracket.shorten-pair #'(0 . -10) %}
    %{ \tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ \tweak HorizontalBracket.edge-height #'(0.5 . 0.5) %}
  \bB k-ah }
  \tuplet 3/2 8 { `r-eh16 \eb n-ih
    \tweak HorizontalBracket.shorten-pair #'(0 . 5)
  \bB n-ah } \tuplet 3/2 8 { `g-ih-v16 \eb m-iy
    \tweak HorizontalBracket.shorten-pair #'(0 . -3)
  \bB Q-ah } \tuplet 3/2 8 { `m-ih16 n-ah-t \eb Q-ah } \tuplet 3/2 8 {
    \tweak HorizontalBracket.shorten-pair #'(0 . -3)
  \bB `m-ay-k8
  Q-ah16 } |
  \tuplet 3/2 8 { `l-ih16 d-el \eb t-uw } \tuplet 3/2 8 {
    \tweak HorizontalBracket.shorten-pair #'(0 . -3)
  \bB `l-ay-k8 g-eh-t16 }
  \tuplet 3/2 8 { `r-ih-d16 Q-ah-v \eb D-ah } \tuplet 3/2 8 {
    \tweak HorizontalBracket.shorten-pair #'(0 . -3)
  \bB `sp-ay-t8 Q-ah16 }
  \tuplet 3/2 8 { `b-ih-t16 Q-ah-v \eb D-ah } \tuplet 3/2 8 {
    \tweak HorizontalBracket.shorten-pair #'(0 . -3)
  \bB `pr-ay-d8 t-ax16 }
  \eb `f-ay-t8 r8 |
  }

songSylsOfficial =
  \lyricmode {
        \bracketSettings
        \override TupletBracket.padding = #0.2
        \override TupletBracket.staff-padding = #3.5
        \override TupletBracket.outside-staff-padding = #0.2
        \override TupletBracket.avoid-scripts = ##t
        \override TupletNumber.whiteout = ##t
        \override Slur.after-line-breaking = #horizontalise-broken-slurs
        \override Slur.avoid-slur = #'inside
        \override PhrasingSlur.avoid-slur = #'inside
        \override PhrasingSlur.layer = #8
        \override TupletNumber.whiteout-style = #'rounded-box
        \override TextScript.padding = #0.5
        \override TextScript.avoid-slur = #'inside
        \phrasingSlurDashed
        \phrasingSlurUp
        \slurDashed
        \slurUp
    \partial 8
    \tuplet 3/2 8 {
  \bB `Q-ay-m8( b-iy16 } |
    \tuplet 3/2 8 {
  `g-ih n-ih-G16 t-uw }
  \eb `r-ay-t8)\(
    \vennBracketA
  `l-ay-k\)\( \tuplet 3/2 8 {
  `Q-ae16 n-ah k-ah }
    \tuplet 3/2 8 {
  `r-eh16 n-ih n-ah }
    \tuplet 3/2 8 {
  g-ih-v16 \eb m-iy
  \vennBracketB Q-ah }
  \tuplet 3/2 8 { `m-ih16 n-ah-t Q-ah } \tuplet 3/2 8 { \eb `m-ay-k8\)( \oB Q-ah16 } |
  \tuplet 3/2 8 { `l-ih16 d-el t-uw } \tuplet 3/2 8 { \eb `l-ay-k8)\( \oB g-eh-t16 }
  \tuplet 3/2 8 { `r-ih-d16 Q-ah-v D-ah } \tuplet 3/2 8 { \eb `sp-ay-t8\)\( \oB Q-ah16 }
  \tuplet 3/2 8 { `b-ih-t16 Q-ah-v D-ah } \tuplet 3/2 8 { \eb `pr-ay-d8\)\( \oB t-uw16 }
  \eb @f-ay-t8\) r8 |
  }

songSylsSluredDomains =
  \lyricmode {
        \bracketSettings
        \override TupletBracket.padding = #0.2
        \override TupletBracket.staff-padding = #3.5
        \override TupletBracket.outside-staff-padding = #0.2
        \override TupletBracket.avoid-scripts = ##t
        \override TupletNumber.whiteout = ##t
        \override TupletNumber.whiteout-style = #'rounded-box
        \phrasingSlurDashed
        \slurDotted
    \partial 8
  \tuplet 3/2 8 { \bB `Q-ay-m8\( b-iy16 } |
  \tuplet 3/2 8 { `g-ih n-ih-G16 t-uw } \eb `r-ay-t8\)\(

  `l-ay-k-\tweak HorizontalBracket.shorten-pair #'(-1.5 . -4)\)\(\startGroup \tuplet 3/2 8 { `Q-ae16 n-ah k-ah }
  \tuplet 3/2 8 { `r-eh16 n-ih n-ah } \tuplet 3/2 8 { `g-ih-v16 \eb m-iy
    \tweak HorizontalBracket.shorten-pair #'(0 . -1)
  Q-ah\startGroup }
  \tuplet 3/2 8 { `m-ih16 n-ah-t Q-ah } \tuplet 3/2 8 { \eb `m-ay-k8\)\( Q-ah16\startGroup } |
  \tuplet 3/2 8 { `l-ih16 d-el t-uw } \tuplet 3/2 8 { \eb `l-ay-k8\)\( g-eh-t16\startGroup }
  \tuplet 3/2 8 { `r-ih-d16 Q-ah-v D-ah } \tuplet 3/2 8 { \eb `sp-ay-t8\)\( Q-ah16\startGroup }
  \tuplet 3/2 8 { `b-ih-t16 Q-ah-v D-ah } \tuplet 3/2 8 { \eb `pr-ay-d8\)\( t-uw16\startGroup }
  \eb @f-ay-t8\) r8 |
  }

songSylsPlain =
  \lyricmode {
        \override TupletBracket.outside-staff-priority = #1100
        \override TupletBracket.padding = #0.2
        \override TupletBracket.staff-padding = #3.5
        \override TupletBracket.outside-staff-padding = #0.2
        \override TupletBracket.avoid-scripts = ##t
        \override TupletNumber.whiteout = ##t
        \override TupletNumber.whiteout-style = #'rounded-box
    \partial 8
  \tuplet 3/2 8 { `Q-ay-m8 b-iy16 } |
  \tuplet 3/2 8 { `g-ih n-ih-G16 t-uw } `r-ay-t8
  `l-ay-k \tuplet 3/2 8 { `Q-ae16 n-ah k-ah }
  \tuplet 3/2 8 { `r-eh16 n-ih n-ah } \tuplet 3/2 8 { `g-ih-v16 m-iy Q-ah }
  \tuplet 3/2 8 { `m-ih16 n-ah-t Q-ah } \tuplet 3/2 8 { `m-ay-k8 Q-ah16 } |
  \tuplet 3/2 8 { `l-ih16 d-el t-uw } \tuplet 3/2 8 { `l-ay-k8 g-eh-t16 }
  \tuplet 3/2 8 { `r-ih-d16 Q-ah-v D-ah } \tuplet 3/2 8 { `sp-ay-t8 Q-ah16 }
  \tuplet 3/2 8 { `b-ih-t16 Q-ah-v D-ah } \tuplet 3/2 8 { `pr-ay-d8 t-uw16 }
  `f-ay-t8 r8 |
  }
\lyricNotation \songLyricsRainbowAlt \songSylsOfficial
\pageBreak
\lyricNotation \songLyricsRainbowAltTwo \songSylsOfficial
\pageBreak
\lyricNotation \songLyricsRainbow \songSylsOfficial
%{ \pageBreak %}
%{ \lyricNotation \songLyricsMBOG \songSyls %}
