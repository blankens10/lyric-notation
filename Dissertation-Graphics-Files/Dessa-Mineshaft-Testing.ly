\version "2.22.0"
\include "../lyric-notation.ily"

songLyricsPlain =
  \lyricmode {
  I'm be -- gin -- ning to write like An -- na Ka -- re -- ni -- na, give me a mi -- nute, a mic
  A li -- ttle to like, get rid of the spite, a bit of the pride to fight
  }

songSylsPlain =
  \lyricmode {
        \override TupletBracket.outside-staff-priority = #1100
        \override TupletBracket.padding = #0.2
        \override TupletBracket.staff-padding = #3.5
        \override TupletBracket.outside-staff-padding = #0.2
        \override TupletBracket.avoid-scripts = ##t
        \override TupletNumber.whiteout = ##t
        \override TupletNumber.whiteout-style = #'rounded-box
    \partial 8
  \tuplet 3/2 8 { `Q-ay-m8 b-iy16 } |
  \tuplet 3/2 8 { `g-ih n-ih-G16 t-uw } `r-ay-t8
  `l-ay-k \tuplet 3/2 8 { `Q-ae16 n-ah k-ah }
  \tuplet 3/2 8 { `r-eh16 n-ih n-ah } \tuplet 3/2 8 { `g-ih-v16 m-iy Q-ah }
  \tuplet 3/2 8 { `m-ih16 n-ah-t Q-ah } \tuplet 3/2 8 { `m-ay-k8 Q-ah16 } |
  \tuplet 3/2 8 { `l-ih16 d-el t-uw } \tuplet 3/2 8 { `l-ay-k8 g-eh-t16 }
  \tuplet 3/2 8 { `r-ih-d16 Q-ah-v D-ah } \tuplet 3/2 8 { `sp-ay-t8 Q-ah16 }
  \tuplet 3/2 8 { `b-ih-t16 Q-ah-v D-ah } \tuplet 3/2 8 { `pr-ay-d8 t-uw16 }
  `f-ay-t8 r8 |
  }

songLyricsRGBRainbow =
  \lyricmode {
        %{ \goodColors %}
        %{ \pinkRainbow %}
            \rhColor Iy "#4dff4d"
            \rhColorGradient Ih "#70db4d" "#90ffff"
            \rhColor Ey "#94b84d"
            \rhColor Eh "#b8944d"
            \rhColor Ae "#db704d"
            \rhColor Ay "#ff4d4d"
            \rhColor Ah "#ff4d4d"
            \rhColor Ax "#ff4d4d"
            \rhColor En "#ff4d4d"
            \rhColor Em "#ff4d4d"
            \rhColor Er "#ff4d4d"
            \rhColor Aw "#ff4d4d"
            \rhColor Aa "#db4d70"
            \rhColor Ao "#b84d94"
            \rhColor Ow "#944db8"
            \rhColor Uh "#704ddb"
            \rhColor El "#704ddb"
            \rhColor Uw "#4d4dff"
            \rhColor IY "#4dff4d"
            \rhColor IH "#70db4d"
            \rhColor EY "#94b84d"
            \rhColor EH "#b8944d"
            \rhColor AE "#db704d"
            \rhColor AY "#ff4d4d"
            \rhColor AH "#ff4d4d"
            \rhColor AX "#ff4d4d"
            \rhColor EN "#ff4d4d"
            \rhColor EM "#ff4d4d"
            \rhColor ER "#ff4d4d"
            \rhColor AW "#ff4d4d"
            \rhColor AA "#db4d70"
            \rhColor AO "#b84d94"
            \rhColor OW "#944db8"
            \rhColor UH "#704ddb"
            \rhColor EL "#704ddb"
            \rhColor UW "#4d4dff"

  I'm\rh AY0 be\rh IY0 --
  gin\rh Ih0 -- ning\rh IH0 to\rh UW0 write\rh Ay0
  like\rh AY0 An\rh AE0 -- na\rh Ax0 Ka\rh AX0 --
  re\rh EH0 -- ni\rh IH0 -- na,\rh AX0 give\rh IH0 me\rh IY0 a\rh AX0
  mi\rh IH0 -- nute,\rh Ax0 a\rh AX0 mic\rh AY0 A\rh AX0 |
  li\rh IH0 -- ttle\rh EL0 to\rh UW0 like,\rh AY0 get\rh EH0
  rid\rh IH0 of\rh Ax0 the\rh AX0 spite,\rh AY0 a\rh AX0
  bit\rh IH0 of\rh Ax0 the\rh AX0 pride\rh AY0 to\rh UW0
  fight\rh AY0 |
  }
songLyricsRainbow =
  \lyricmode {
      \rgbRainbowGradients

  I'm\rh AY0 be\rh IY0 --
  gin\rh Ih0 -- ning\rh IH0 to\rh UW0 write\rh Ay0
  like\rh AY0 An\rh AE0 -- na\rh Ax0 Ka\rh AX0 --
  re\rh EH0 -- ni\rh IH0 -- na,\rh AX0 give\rh IH0 me\rh IY0 a\rh AX0
  mi\rh IH0 -- nute,\rh Ax0 a\rh AX0 mic\rh AY0 A\rh AX0 |
  li\rh IH0 -- ttle\rh EL0 to\rh UW0 like,\rh AY0 get\rh EH0
  rid\rh IH0 of\rh Ax0 the\rh AX0 spite,\rh AY0 a\rh AX0
  bit\rh IH0 of\rh Ax0 the\rh AX0 pride\rh AY0 to\rh UW0
  fight\rh AY0 |
  }
songLyricsRainbowTest =
  \lyricmode {
      \rgbRainbowGradients

  I'm\rh AY0 be\rh IY0 -- |
  gin\rh ih0 -- ning\rh IH0 to\rh UW0 write\rh ay0
  like\rh AY0 An\rh AE0 -- na\rh Ax0 Ka\rh AX0 --
  re\rh EH0 -- ni\rh IH0 -- na,\rh AX0 give\rh IH0 me\rh IY0 a\rh AX0
  mi\rh IH0 -- nute,\rh Ax0 a\rh AX0 mic\rh AY0 A\rh AX0 |
  li\rh IH0 -- ttle\rh EL0 to\rh UW0 like,\rh AY0 get\rh EH0
  rid\rh IH0 of\rh Ax0 the\rh AX0 spite,\rh AY0 a\rh AX0
  bit\rh IH0 of\rh Ax0 the\rh AX0 pride\rh AY0 to\rh UW0
  fight\rh AY0 |
}

songSylsBrackets =
  \lyricmode {
      \bracketSettings
        %{ \override TupletBracket.outside-staff-priority = #1100 %}
        %{ \override TupletBracket.padding = #0.2 %}
        %{ \override TupletBracket.staff-padding = #3.5 %}
        %{ \override TupletBracket.outside-staff-padding = #0.2 %}
        %{ \override TupletBracket.avoid-scripts = ##f %}
        %{ \override TupletNumber.whiteout = ##t %}
        %{ \override TupletNumber.whiteout-style = #'rounded-box %}

    \partial 8
  \tuplet 3/2 8 { \bB `Q-ay-m8 b-iy16 } |
  \tuplet 3/2 8 { `g-ih n-ih-G16 t-uw } \eb `r-ay-t8
  `l-ay-k \tuplet 3/2 8 {
  `Q-ae16 n-ah
    %{ \tweak HorizontalBracket.shorten-pair #'(0 . -10) %}
    %{ \tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ \tweak HorizontalBracket.edge-height #'(0.5 . 0.5) %}
  \bB k-ah }
  \tuplet 3/2 8 { `r-eh16 \eb n-ih
    \tweak HorizontalBracket.shorten-pair #'(0 . 5)
  \bB n-ah } \tuplet 3/2 8 { `g-ih-v16 \eb m-iy
    \tweak HorizontalBracket.shorten-pair #'(0 . -3)
  \bB Q-ah } \tuplet 3/2 8 { `m-ih16 n-ah-t \eb Q-ah } \tuplet 3/2 8 {
    \tweak HorizontalBracket.shorten-pair #'(0 . -3)
  \bB `m-ay-k8
  Q-ah16 } |
  \tuplet 3/2 8 { `l-ih16 d-el \eb t-uw } \tuplet 3/2 8 {
    \tweak HorizontalBracket.shorten-pair #'(0 . -3)
  \bB `l-ay-k8 g-eh-t16 }
  \tuplet 3/2 8 { `r-ih-d16 Q-ah-v \eb D-ah } \tuplet 3/2 8 {
    \tweak HorizontalBracket.shorten-pair #'(0 . -3)
  \bB `sp-ay-t8 Q-ah16 }
  \tuplet 3/2 8 { `b-ih-t16 Q-ah-v \eb D-ah } \tuplet 3/2 8 {
    \tweak HorizontalBracket.shorten-pair #'(0 . -3)
  \bB `pr-ay-d8 t-ax16 }
  \eb `f-ay-t8 r8 |
  }

songSylsBracketsAndSlurs =
  \lyricmode {
        \bracketSettings
        \override TupletBracket.padding = #0.2
        \override TupletBracket.staff-padding = #3.5
        \override TupletBracket.outside-staff-padding = #0.2
        \override TupletBracket.avoid-scripts = ##t
        \override TupletNumber.whiteout = ##t
        \override TupletNumber.whiteout-style = #'rounded-box
        \phrasingSlurDashed
    \partial 8
  \tuplet 3/2 8 { \bB `Q-ay-m8\( b-iy16 } |
  \tuplet 3/2 8 { `g-ih n-ih-G16 t-uw } \eb `r-ay-t8\)\(

  `l-ay-k-\tweak HorizontalBracket.shorten-pair #'(-1.5 . -4)\)\(\startGroup \tuplet 3/2 8 { `Q-ae16 n-ah k-ah }
  \tuplet 3/2 8 { `r-eh16 n-ih n-ah } \tuplet 3/2 8 { `g-ih-v16 \eb m-iy
    \tweak HorizontalBracket.shorten-pair #'(0 . -1)
  Q-ah\startGroup }
  \tuplet 3/2 8 { `m-ih16 n-ah-t Q-ah } \tuplet 3/2 8 { \eb `m-ay-k8\)\( Q-ah16\startGroup } |
  \tuplet 3/2 8 { `l-ih16 d-el t-uw } \tuplet 3/2 8 { \eb `l-ay-k8\)\( g-eh-t16\startGroup }
  \tuplet 3/2 8 { `r-ih-d16 Q-ah-v D-ah } \tuplet 3/2 8 { \eb `sp-ay-t8\)\( Q-ah16\startGroup }
  \tuplet 3/2 8 { `b-ih-t16 Q-ah-v D-ah } \tuplet 3/2 8 { \eb `pr-ay-d8\)\( t-uw16\startGroup }
  \eb @f-ay-t8\) r8 |
  }
\lyricNotation \songLyricsRainbow \songSylsBracketsAndSlurs
\pageBreak
\lyricNotation \songLyricsRainbowTest \songSylsBracketsAndSlurs
%{ \pageBreak %}
%{ \lyricNotation \songLyricsMBOG \songSyls %}
