\version "2.22.0"
\include "../lyric-notation.ily"

songLyricsPlain =
  \lyricmode {
    Catch a throat -- ful from the fi -- re vo -- caled
    With ash and mol -- ten glass like Ey -- jaf -- jal -- la -- jö -- kull}

songLyricsMBOG =
  \lyricmode {
    \rhColor ax #(x11-color 'Khaki)
    \rhColor a #(x11-color 'Tomato)
    \rhColor A #(x11-color 'Tomato)
    \rhColor b #(x11-color 'IndianRed)
    \rhColor c #(x11-color 'LightGreen)
    \rhColor d #(x11-color 'LightSeaGreen)

  Catch\rh d0 a\rh ax0 throat\rh b1 -- ful\rh b2 from\rh ax0 the\rh ax0 fi\rh a1 -- re\rh a2 vo\rh b1 -- caled\rh b2
  With\rh c0 ash\rh d0 and\rh ax0 mol\rh b1  -- ten\rh c0 glass\rh d0 like\rh A1 Ey\rh a1 -- jaf\rh a2 -- jal\rh A1 -- la\rh A2 -- jö\rh b1 -- kull\rh b2}

songLyricsMB =
  \lyricmode {
    \rhColor ax #(x11-color 'Khaki)
    \rhColor a #(x11-color 'Tomato)
    \rhColor A #(x11-color 'Tomato)
    \rhColor b #(x11-color 'IndianRed)
    \rhColor c #(x11-color 'LightGreen)
    \rhColor d #(x11-color 'LightSeaGreen)

  Catch\rh d0 a\rh ax0 throat\rh b0 -- ful\rh b0 from\rh ax0 the\rh ax0 fi\rh a0 -- re\rh a0 vo\rh b0 -- caled\rh b0
  With\rh c0 ash\rh d0 and\rh ax0 mol\rh b0  -- ten\rh c0 glass\rh d0 like\rh A0 Ey\rh a0 -- jaf\rh a0 -- jal\rh A0 -- la\rh A0 -- jö\rh b0 -- kull\rh b0}


songLyricsRainbow =
  \lyricmode {
      \goodColors
      \pinkRainbow
    Catch\rh AE0
    a\rh AX0
    throat\rh OW0
     -- ful\rh EL0
    from\rh EM0
    the\rh AX0
    fi\rh AY0
     -- re\rh ER0
    vo\rh OW0
     -- caled\rh EL0
    With\rh Ih0
    ash\rh AE0
    and\rh EN0
    mol\rh OW0
     -- ten\rh IH0
    glass\rh AE0
    like\rh AY0
    Ey\rh Ay0
     -- jaf\rh AH0
     -- jal\rh Ay0
     -- la\rh AH0
     -- jö\rh OW0
     -- kull\rh EL0
}

songSylsLabeled =
  \lyricmode {

    \phrasingSlurDashed
    r8 \bB `k-ae-C16(\( Q-ah @Tr-ow-t8 \eb f-el) \bB `fr-em16( D-ah `f-ay Q-er @v-ow8 \eB k-el-d16)\)
    \bB w-ih-D(\( |
    \eb @Q-ae-S8 \bB
    \bB Q-en16 \eb `m-ow-L) \bB t-ih-n16( \eb \eb @gl-ae-s8 \bB l-ay-k16 `Q-ay y-ax-f `J-ay l-ax @J-ow \eB k-el)\) r8 |
    }
songSylsPlain =
  \lyricmode {
  r8 `k-ae-C16 Q-ah @Tr-ow-t8 f-el `fr-ah-m16 D-ah `f-ay Q-er @v-ow8 k-el16 w-ih-D |
  @Q-ae-S8 Q-en16 `m-ow-l t-ih-n16 @gl-ae-s8 l-ay-k16 `Q-ay y-ax-f `J-ay l-ax @J-ow k-el r8 | }

songSylsGrooves =
  \lyricmode {
          %{ \bracketSettings %}
          \genGrooveTupSettings
  \gClass "<2222_2222>-0" { \gTwo { r8 } \gTwo { `k-ae-C16 Q-ah } \gTwo { @Tr-ow-t8 } \gTwo { f-el } \gTwo { `fr-ah-m16 D-ah } \gTwo { `f-ay Q-er } \gTwo { @v-ow8 } \gTwo { k-el16 w-ih-D } } |
  \gClass "<323_2222>-0" { \gTre { @Q-ae-S8 Q-en16 } \gTwo { `m-ow-l t-ih-n16 } \gTre { @gl-ae-s8 l-ay-k16 } \gTwo { `Q-ay y-ax-f } \gTwo { `J-ay l-ax } \gTwo { @J-ow k-el } \gTwo { r8 } } |
}

%{ \lyricNotation \songLyricsRainbow \songSylsPlain \pageBreak %}
  %{ \oneLineLyricNotation \songLyricsRainbow \songSylsPlain \pageBreak %}
%{ \lyricNotation \songLyricsRainbow \songSylsLabeled \pageBreak %}
  %{ \oneLineLyricNotation \songLyricsRainbow \songSylsLabeled \pageBreak %}
\lyricNotation \songLyricsMB \songSylsLabeled
%{ \pageBreak %}
  %{ \oneLineLyricNotation \songLyricsMBOG \songSylsLabeled \pageBreak %}
%{ \oneLineLyricNotation \songLyricsMB \songSylsGrooves %}
%{ \pageBreak %}
%{ \lyricNotation \songLyricsMBOG \songSylsLabeled %}
