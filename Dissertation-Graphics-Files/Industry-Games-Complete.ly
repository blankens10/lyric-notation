\version "2.22.0"
\include "../lyric-notation.ily"

songLyricsPlain =
    \lyricmode {
    Nig -- gas be playin', yeah
    In -- dus -- try games, yeah
    Rap -- pin' but they not in love with it
    Think it's a shame, yeah
    I'm 'bout to pop on some other shit
    Think it's a game?
    They don't con -- trol where I land
    Roy -- al flush, nig -- ga the cards in my hand }
songSylsPlain =
  \lyricmode {
  r8 \tuplet 3/2 8 { `n-ih16 g-ax-z b-iy } <>`pl-ey-n8 y-ae '
  r8 \tuplet 3/2 8 { `Q-ih-n16 d-ax-s tr-iy } `g-ey-mz8 y-ae ' |
  r8 \tuplet 3/2 8 { `r-ae16 p-ih-n b-ah-t `D-ey n-aa-t Q-ih-n `l-ah-v w-ih-D Q-ih-t } '
  r8 \tuplet 3/2 8 { `T-iy-Gk16 Q-ih-ts Q-ah } `S-ey-m8 y-ae8 '
  r8 \tuplet 3/2 8 { `Q-ay-m16 b-aw-t t-uw `p-aa-p Q-aa-n s-ah-m `Q-ah D-er S-ih-t }
  r8 \tuplet 3/2 8 { `T-iy-Gk16 Q-ih-ts Q-ah } `g-ey-m8 ' r8
  \tuplet 3/2 8 { `D-ey16 d-ow-nt k-ah-n `tr-el w-eh-r Q-ay } `l-ae-nd8 ' r8
  \tuplet 3/2 8 { `r-oy16 y-el fl-ah-S `n-ih g-ah D-ah `k-aa-rdz Q-ih-n m-ay } `h-ae-nd8
  }

songLyricsRhymes =
      \lyricmode {
        \rhColor a "#94b84d"
        \rhColor A "#A5CC00"
        \rhColor b "#704ddb"
        \rhColor c "#A5CC00"
        \rhColor d "#3300cc"

      Nig\rh a1 -- gas\rh a2 be\rh a3 playin',\rh b1 yeah\rh b2
      In\rh a1 -- dus\rh a2 -- try\rh a3 games,\rh b1 yeah\rh b2
      Rap -- pin' but they not in love with it
      Think\rh c3 it's\rh c1 a\rh c2 shame,\rh b1 yeah\rh b2
      %{ I'm 'bout to pop on some o -- ther shit
      Think\rh c1 it's\rh c2 a\rh c3 game?\rh b1
      They don't con -- trol where I\rh c3 land\rh d1
      Roy -- al flush, nig -- ga the cards in\rh c2 my\rh c3 hand\rh d1  %}
    }

songSylsLabeled =
  \lyricmode {
      %{ \override Score.TupletBracket.visibility = ##f %}
      %{ \override Score.TupletBracket.transparent = ##t %}
      %{ \override Score.TupletBracket.padding = #4 %}
      \override TupletBracket.stencil = ##f
      \override TupletNumber.stencil = ##f

    r8
    \tuplet 3/2 { `n-ih16 g-ax-z b-iy } <>`pl-ey-n8 y-ae '
    r8 \tuplet 3/2 { `Q-ih-n16 d-ax-s tr-iy } `g-ey-mz8 y-ae ' |
    r8 \tuplet 3/2 { `r-ae16 p-ih-n b-ah-t }
      \tuplet 3/2 { `D-ey n-aa-t Q-ih-n }
        \tuplet 3/2 { `l-ah-v w-ih-D Q-ih-t } '
    r8 \tuplet 3/2 { `T-iy-Gk16 Q-ih-ts Q-ah } `S-ey-m8 y-ae8 '
    %{ r8 \tuplet 3/2 { `Q-ay-m16 b-aw-t t-uw 3/2 } { `p-aa-p Q-aa-n s-ah-m 3/2 } { `Q-ah D-er S-ih-t }
    r8 \tuplet 3/2 { `T-iy-Gk16 Q-ih-ts Q-ah } `g-ey-m8 ' r8
    \tuplet 3/2 8 { `D-ey16 d-ow-nt k-ah-n `tr-el w-eh-r Q-ay } `l-ae-nd8 ' r8
    \tuplet 3/2 8 { `r-oy16 y-el fl-ah-S `n-ih g-ah D-ah `k-aa-rdz Q-ih-n m-ay } `h-ae-nd8 %}
  }
oLsongLyricsRhymes =
      \lyricmode {
        \rhColor a "#94b84d"
        \rhColor A "#bfdca6"
        \rhColor b "#704ddb"
        \rhColor c "#33cc00"
        \rhColor d "#3300cc"

      Nig -- gas be playin',\rh b0 yeah\rh b0
      In -- dus -- try games,\rh b0 yeah\rh b0
      Rap -- pin' but they not in love with it
      Think it's a shame,\rh b0 yeah\rh b0
      %{ I'm 'bout to pop on some o -- ther shit
      Think it's a game?\rh b
      They don't con -- trol where I\rh c3 land\rh d1
      Roy -- al flush, nig -- ga the cards in\rh c2 my\rh c3 hand\rh d1  %}
    }

oLsongSylsLabeled =
  \lyricmode {
      %{ \override Score.TupletBracket.visibility = ##f %}
      %{ \override Score.TupletBracket.transparent = ##t %}
      %{ \override Score.TupletBracket.padding = #4 %}
      \override TupletBracket.stencil = ##f
      \override TupletNumber.stencil = ##t
      \headphonesOff

    r8
    \tuplet 3/2 { `n-ih16 g-ax-z b-iy } `pl-eh-n8 y-ae '
    r8 \tuplet 3/2 { `Q-ih-n16 d-ax-s tr-iy } `g-eh-mz8 y-ae ' |
    r8 \tuplet 3/2 { `r-ae16 p-ih-n b-ah-t }
      \tuplet 3/2 { `D-eh n-aa-t Q-ih-n }
        \tuplet 3/2 { `l-ah-v w-ih-D Q-ih-t } '
    r8 \tuplet 3/2 { `T-iy-Gk16 Q-ih-ts Q-ah } `S-eh-m8 y-ae8 '
    %{ r8 \tuplet 3/2 { `Q-ay-m16 b-aw-t t-uw 3/2 } { `p-aa-p Q-aa-n s-ah-m 3/2 } { `Q-ah D-er S-ih-t }
    r8 \tuplet 3/2 { `T-iy-Gk16 Q-ih-ts Q-ah } `g-ey-m8 ' r8
    \tuplet 3/2 8 { `D-ey16 d-ow-nt k-ah-n `tr-el w-eh-r Q-ay } `l-ae-nd8 ' r8
    \tuplet 3/2 8 { `r-oy16 y-el fl-ah-S `n-ih g-ah D-ah `k-aa-rdz Q-ih-n m-ay } `h-ae-nd8 %}
  }

\lyricNotation \songLyricsRhymes \songSylsLabeled
\pageBreak
\oneLineLyricNotation \oLsongLyricsRhymes \oLsongSylsLabeled
