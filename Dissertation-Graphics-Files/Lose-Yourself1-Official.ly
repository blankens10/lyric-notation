\version "2.22.0"
\include "../lyric-notation.ily"

songLyricsRainbow =
  \lyricmode {
    \rgbRainbowGradients


  % Pickup
  His\rh ih0 palms\rh aa0
  % Bar 1
  are\rh aar0 swea\rh eh0 -- ty,\rh iy0
  knees\rh IY0 weak,\rh iy0
  arms\rh aar0 are\rh AAR0 hea\rh eh0 --
  vy\rh iy0 There's\rh ehr0 vo\rh aa0 -- mit\rh ih0
  % Bar 2
  on\rh aa0 his\rh ih0 swea\rh eh0 -- ter\rh er0
  al\rh aa0 -- rea\rh eh0 -- dy,\rh iy0
  mom's\rh ao0 spa\rh ah0 -- ghe\rh eh0 --
  tti\rh iy0 He's\rh IY0 ner\rh er0 -- vous\rh ax0
  % Bar 3
  but\rh AX0 on\rh aa0 the\rh ax0
  sur\rh er0 -- face\rh ax0 he\rh iy0 looks\rh uh0
  calm\rh aa0 and\rh ae0 rea\rh eh0 --
  dy\rh iy0 To\rh uw0
  drop\rh aa0
  % Bar 4
  bombs\rh AA0
  but\rh ax0 he\rh iy0 keeps\rh IY0
  on\rh aa0 for\rh aor0 -- ge\rh eh0 --
  tting\rh ih0 What\rh ah0 he\rh iy0 wrote\rh ow0
  % Bar 5
  dowon,\rh aw0 the\rh ax0
  whole\rh ow0 crowd\rh aw0
  goes\rh ow0 so\rh OW0 loud\rh aw0
  He\rh ih0 o\rh ow0 --
  % Bar 6
  pens\rh ih0 his\rh IH0 mouth,\rh aw0
  but\rh ah0 the\rh ax0 words\rh er0
  won't\rh ow0 come\rh ax0 out\rh aw0
  He's\rh iy0 cho\rh ow0 --
  % Bar 7
  king,\rh ih0 how?\rh aw0
  Eve\rh eh0 -- ry\rh iy0 -- bo\rh ah0 -- dy's\rh iy0
  jo\rh ow0 -- king\rh ih0 now\rh aw0
  The\rh ax0 clock's\rh ow0
  % Bar 8
  run\rh ax0 out,\rh aw0
  time's\rh ay0 up,\rh ah0
  o\rh ow0 -- ver,\rh er0 blaow!\rh aw0
  Snap\rh ae0
  % Bar 9
  back\rh AE0 to\rh uw0 re\rh iy0 --
  a\rh ae0 -- li\rh ih0 -- ty,\rh iy0 oh\rh ow0
  there\rh ehr0 goes\rh ow0
  gra\rh ae0 -- vi\rh ih0 -- ty,\rh iy0 oh\rh ow0
  % Bar 10
  There\rh ehr0 goes\rh ow0
  Ra\rh ae0 -- bbit,\rh ih0 he\rh iy0 choked,\rh ow0
  he's\rh iy0 so\rh ow0
  mad\rh ae0 but\rh ax0 he\rh iy0 won't\rh ow0
  % Bar 11
  Give\rh ih0 up\rh ow0
  that\rh ae0 ea\rh iy0 -- sy,\rh IY0 no,\rh ow0
  he\rh iy0 won't\rh ow0
  have\rh ae0 it,\rh ih0 he\rh iy0 knows\rh ow0
  % Bar 12
  His\rh ih0 whole\rh ow0
  back's\rh ae0 to\rh uw0 these\rh iy0 ropes,\rh ow0
  it\rh ih0 don't\rh ow0
  ma\rh ae0 -- tter,\rh er0 he's\rh iy0 dope\rh ow0
  % Bar 13
  He\rh iy0 knows\rh ow0
  that\rh ae0 but\rh ax0 he's\rh iy0 broke,\rh ow0
  he's\rh iy0 so\rh ow0
  stag\rh ae0 -- nant,\rh AE0 he\rh iy0 knows\rh ow0
  % Bar 14
  When\rh eh0 he\rh iy0 goes\rh ow0
  back\rh ae0 to\rh uw0 this\rh ih0 mo\rh ow0 --
  bile\rh el0 home,\rh ow0
  that's\rh ae0 when\rh eh0 it's\rh ih0
  % Bar 15
  Back\rh ae0 to\rh uw0 the\rh ax0
  lab\rh ae0 a\rh ax0 -- gain\rh ih0 yo,\rh ow0
  this\rh ih0 whole\rh ow0
  rhap\rh ae0 -- so\rh ow0 -- dy,\rh iy0 Bet\rh eh0 --
  % Bar 16
  ter\rh er0 go\rh ow0 cap\rh ae0 -- ture\rh er0
  this\rh ih0 mo\rh ow0 -- ment\rh eh0 and\rh ax0
  hope\rh ow0 it\rh ih0 don't\rh ow0
  pass\rh ae0 him\rh ih0
  }

songSylsPlain =
  \lyricmode {
  \partial 4
  % Pickup
  r16 h-ih-z16 @p-aa-lmz8 |
  % Bar 1
  Q-aa-r16 `sw-eh -- t-iy8
  @n-iy-z `w-iy-k
  @Q-aa-rm z-aa-r16 `h-eh --
  v-iy D-eh-rz `v-aa -- m-ih-t |
  % Bar 2
  `Q-aa-n h-ih-z `sw-eh -- t-er
  Q-aa-l -- `r-eh -- d-iy8
  @m-aa-mz sp-ah16 -- `g-eh --
  t-iy16 h-iy-z `n-er -- v-ah-s |
  % Bar 3
  r16. b-ah-t32 `Q-aa-n16 D-ah
  `s-er -- f-ah-s `h-iy l-uh-ks
  @k-aa-lm8 Q-ah-nd16 `r-eh --
  d-iy t-uw `dr-aa-p8 |
  % Bar 4
  @b-aa-mz8 r8
  `b-ah-t16 h-iy `k-iy-ps8
  @Q-aa-n f-er16 -- `g-eh --
  t-ih-G16 `w-ah-t h-iy `r-ow-t |
  % Bar 5
  _16 @d-aw-n8 D-ah16
  `h-ow-l8 @kr-aw-d
  `g-ow-z s-ow16 `l-aw-d
  _16  h-iy `y-ow8 -- |
  % Bar 6
  p-ih-nz16 h-ih-z `m-aw-T8
  `b-ah-t16 D-ah `w-er-dz8
  `w-ow-nt k-ah16 `m-aw-t
  r16 h-iy-z `C-ow8 -- |
  % Bar 7
  k-ih-G16 @h-aw8 r16
  `Q-eh-v16 -- r-iy -- `b-ah -- d-iy-z
  `J-ow8 -- k-ih-G16 @n-aw
  r16 D-ah `kl-aa-ks8 |
  % Bar 8
  r-ah-n16 @Q-aw-t8 r16
  `t-ay-mz8 Q-ah-p
  `Q-ow -- v-er16 @bl-aw16
  _16 r16 `sn-ae-p8 |
  % Bar 9
  @b-ae-k8 t-uw16 r-iy --
  `y-ae l-ih -- t-iy @Q-ow
  r16 D-eh-r16 `g-ow-z8
  `gr-ae16 -- v-ih -- t-iy @Q-ow |
  % Bar 10
  r16 D-eh-r16 `g-ow-z8
  `r-ae16 -- b-ih-t h-iy @C-ow-kt
  r16 h-iy-z16 `s-ow8
  `m-ae-d16 b-ah-t h-iy @w-ow-nt |
  % Bar 11
  _16   g-ih16 `<>v-uh-p8
  `D-ae-t16 Q-iy -- z-iy @n-ow
  r16 h-iy `w-ow-nt8
  `h-ae16 v-ih-t h-iy @n-ow-z |
  % Bar 12
  _16 h-ih-z `h-ow-l8
  `b-ae-ks16 t-ah D-iy-z @r-ow-ps
  r16 Q-ih-t `d-ow-nt8
  `m-ae16 -- t-er h-iy-z @d-ow-p |
  % Bar 13
  r16 h-iy `n-ow-z8
  `D-ae-t16 b-ah-t h-iy-z @br-ow-k
  r16 h-iy-z `s-ow8
  `st-ae-g16 -- n-ih-nt h-iy @n-ow-z |
  % Bar 14
  _16 w-eh-n32 h-iy `g-ow-z8
  \tuplet 3/2 8 { `b-ae-k8 t-ah16 } D-ih-s16 @m-ow --
  _16 b-el `h-ow-m8
  \tuplet 3/2 8 { `D-ae-ts8 w-eh-n16 } Q-ih-ts16 r16 |
  % Bar 15
  @b-ae-k8 t-uw16 D-ah
  `l-ae b-ah -- g-ih-n @y-ow
  r16 D-ih-s `h-ow-l8
  `r-ae-p16 -- s-ih -- d-iy `b-eh -- |
  % Bar 16
  t-er g-ow `k-ae-p -- C-er
  D-ih-s `m-ow -- m-eh-nt Q-ax-nd
  `h-ow p-ih-t `d-ow-nt8
  @p-ae-s16 h-ih-m r8 |
  }

songLyricsDomains =
  \lyricmode {
    \rhColor a #(x11-color 'LightSeaGreen)
    \rhColor A #(x11-color 'LightSeaGreen)
    \rhColor b #(x11-color 'LightGreen)
    \rhColor B #(x11-color 'LightGreen)
    \rhColor c #(x11-color 'LawnGreen)
    \rhColor C #(x11-color 'LawnGreen)
    \rhColor i #(x11-color 'GreenYellow)
    \rhColor iii #(x11-color 'MediumSeaGreen)
    \rhColor d #(x11-color 'Gold)
    %{ \rhColor ax #(x11-color 'Khaki) %}
    \rhColor ii #(x11-color 'DarkKhaki)
    \rhColor f #(x11-color 'Tomato)
    \rhColor g #(x11-color 'IndianRed)
    \rhColor h #(x11-color 'Magenta)
    \rhColor H #(x11-color 'Orchid)
    \rhColor HH #(x11-color 'Orchid)
    \rhColor ah #(x11-color 'Khaki)
    \rhColor ax #(x11-color 'PaleGoldenrod)
    \rhColor er #(x11-color 'BurlyWood2)
    \rhColor ER #(x11-color 'BurlyWood1)
    \rhColor eh #(x11-color 'Green3)
    \rhColor ehr #(x11-color 'Green4)



  % Pickup
  His\rh i0 palms\rh a0
  % Bar 1
  are\rh b1 swea\rh b2 -- ty,\rh b3
  knees\rh c0 weak,\rh C0
  arms\rh a0 are\rh b0 hea\rh b0 --
  vy\rh b0 There's\rh ehr0 vo\rh a0 -- mit\rh i0
  % Bar 2
  on\rh a0 his\rh i0 swea\rh B2 -- ter\rh B1
  al\rh b0 -- rea\rh b0 -- dy,\rh b0
  mom's\rh a0 spa\rh b0 -- ghe\rh b0 --
  tti\rh b0 He's\rh i0 ner\rh er0 -- vous\rh ax0
  % Bar 3
  but\rh ax0 on\rh a0 the\rh ax0
  sur\rh er0 -- face\rh ax0 he\rh c0 looks\rh iii0
  calm\rh a0 and\rh b0 rea\rh b0 --
  dy\rh b0 To\rh iii0
  drop\rh a0
  % Bar 4
  bombs\rh a0
  but\rh b1 he\rh b3 keeps\rh b3
  on\rh a0 for\rh b0 -- ge\rh b0 --
  tting\rh b0 What\rh ah0 he\rh i0 wrote\rh f0
  % Bar 5
  down,\rh g0 the\rh ax0
  whole\rh f0 crowd\rh g0
  goes\rh f0 so\rh f0 loud\rh g0
  He\rh i0 o\rh f0 --
  % Bar 6
  pens\rh i0 his\rh i0 mouth,\rh g0
  but\rh ah0 the\rh ax0 words\rh er0
  won't\rh f0 come\rh ax0 out\rh g0
  He's\rh i0 cho\rh f0 --
  % Bar 7
  king,\rh i0 how?\rh g0
  Eve\rh eh0 -- ry\rh i0 -- bo\rh ah0 -- dy's\rh i0
  jo\rh f0 -- king\rh i0 now\rh g0
  The\rh ax0 clock's\rh f0
  % Bar 8
  run\rh ax0 out,\rh g0
  time's\rh i0 up,\rh ah0
  o\rh f0 -- ver,\rh er0 blaow!\rh g0
  Snap\rh h0
  % Bar 9
  back\rh H0 to\rh H0 re\rh H0 --
  a\rh h0 -- li\rh h0 -- ty,\rh h0 oh\rh f0
  there\rh ehr0 goes\rh f0
  gra\rh h0 -- vi\rh h0 -- ty,\rh h0 oh\rh f0
  % Bar 10
  There\rh ehr0 goes\rh f0
  Ra\rh h0 -- bbit,\rh h0 he\rh h0 choked,\rh f0
  he's\rh i0 so\rh f0
  mad\rh H0 but\rh H0 he\rh H0 won't\rh f0
  % Bar 11
  Give\rh i0 up\rh f0
  that\rh h0 ea\rh h0 -- sy,\rh h0 no,\rh f0
  he\rh i0 won't\rh f0
  have\rh h0 it,\rh h0 he\rh h0 knows\rh f0
  % Bar 12
  His\rh i0 whole\rh f0
  back's\rh H0 to\rh H0 these\rh H0 ropes,\rh f0
  it\rh i0 don't\rh f0
  ma\rh H0 -- tter,\rh H0 he's\rh H0 dope\rh f0
  % Bar 13
  He\rh i0 knows\rh f0
  that\rh H0 but\rh H0 he's\rh H0 broke,\rh f0
  he's\rh i0 so\rh f0
  stag\rh h0 -- nant,\rh h0 he\rh h0 knows\rh f0
  % Bar 14
  When\rh eh0 he\rh i0 goes\rh f0
  back\rh H0 to\rh H0 this\rh H0 mo\rh f0 --
  bile\rh iii0 home,\rh f0
  that's\rh h0 when\rh h0 it's\rh h0
  % Bar 15
  Back\rh H0 to\rh H0 the\rh H0
  lab\rh HH0 a\rh HH0 -- gain\rh HH0 yo,\rh f0
  this\rh i0 whole\rh f0
  rhap\rh h0 -- so\rh h0 -- dy,\rh h0 Bet\rh eh0 --
  % Bar 16
  ter\rh ER0 go\rh f0 cap\rh H0 -- ture\rh H0
  this\rh H0 mo\rh f0 -- ment\rh eh0 and\rh ax0
  hope\rh f0 it\rh c0 don't\rh f0
  pass\rh h1 him\rh h3
  }

songSylsNew =
  \lyricmode {
    \partial 4
     %{ \set Score.tupletFullLength = ##t %}
    \bracketSettings
    %{ \bSet %}
    \bracketTextBeforeBreak
    \override Score.AnalysisNumber.avoid-slur = #'outside
    %{ \override Score.AnalysisNumber.outside-staff-priority = ##f %}
    %{ \override Score.AnalysisNumber.direction = #DOWN %}
  % Pickup
    \phrasingSlurDashed \slurDotted
                r16
                \domTupLabel "[a1]"
                \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                %\multiSet
                \innerNestedBracket "⟦ab1⟧"
    % his palms
    h-ih-z16( % \finger \markup { \teeny \smaller \smaller \smaller "i"}
    @p-aa-lmz8) } |
      % Bar 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                \domTupLabel "[b1]"
                \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % are


    %{ \once \override Fingering.direction = #down %}
    Q-aa-r16(
    %{  % \finger \markup { \teeny \smaller \smaller \smaller  "1"} %}
    % swea-ty
    `sw-eh -- \eB t-iy8)}
                \domTupLabel "[c1]"
                \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % knees
    @n-iy-z( }
                \domTupLabel "[c2]"
                \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % weak
    `w-iy-k) }
                \domTupLabel "[A2]"
                \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                    %\multiSet
                \innerNestedBracket "⟦AB2⟧"
    % arms
    @Q-aa-rm }
                    \domTupLabel "[b2]"
                    \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % are
    z-aa-r16(
    % hea-
    `h-eh --
                    \eB
    % -vy
    v-iy) }
                    \domTupLabel "[a⟺3]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                    %\multiSet
                    \onceBracketTextAfterBreak
                    %{ \once \override Score.HorizontalBracketText.direction = #1 %}
                    \outerNestedBracket "⟦aabb3⟧"
    % theres
    D-eh-rz( % \finger \markup { \teeny \smaller \smaller \smaller  "ii"}

                    \innerNestedBracket "⟦aa1⟧"
    % vo-mit
    `v-aa -- m-ih-t) % \finger \markup { \teeny \smaller \smaller \smaller "i"}
     }
    | % Bar 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    \domTupLabel "[a⟺4]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % on his
    `Q-aa-n( \eb h-ih-z) % \finger \markup { \teeny \smaller \smaller \smaller "i"}
     }
                    \domTupLabel "[b⟺3]"
                    \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % swea-ter
    `sw-eh( -- t-er) }
                    \domTupLabel "[b4]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % all
    Q-aa-l( --
    % rea-dy
    `r-eh -- \eB d-iy8) }
                    \domTupLabel "[A5]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                    %\multiSet
                    \innerNestedBracket "⟦AB4⟧"
    % mom's spa-
    @m-aa-mz }
                    \domTupLabel "[b5]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % spa-
    sp-ah16( --
    % -ghe
    `g-eh --
                    \eB
    % -ti
    t-iy16) }
                    \domTupLabel "[d1]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % he's
    h-iy-z( % \finger \markup { \teeny \smaller \smaller \smaller "i"}

    % ner-vous
    `n-er -- v-ah-s) } |
    % Bar 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        r16.
                    \domTupLabel "[a6]" \boldTup
                \smallTup
                \whiteoutTup
                \lowerTup
                \upTup {
    % but
    b-ah-t32( % \finger \markup { \teeny \smaller \smaller \smaller  "ə"}
    % on
    `Q-aa-n16) }
                    \domTupLabel "[d2]"
                    \boldTup
                    \smallTup
                    \whiteoutTup
                    \lowerTup
                    \upTup {
    % the
    D-ah( % \finger \markup { \teeny \smaller \smaller \smaller  "ə"}
    % sur-face
    `s-er -- f-ah-s) }
                    %{ \domTupLabel "c3" \boldTup
                \smallTup
                \whiteoutTup
                \upTup { %}
                    \domTupLabel "[A7]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % he
    h-iy( % \finger \markup { \teeny \smaller \smaller \smaller "i"}

                    %{ \domTupLabel "A7" \boldTup
                \smallTup
                \whiteoutTup
                \upTup { %}
    % looks
    l-uh-ks
                    %\multiSet
                    \innerNestedBracket "⟦AB5⟧"
    % calm and
    @k-aa-lm8) }
                    \domTupLabel "[B6]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % and
    Q-ah-nd16(
    % rea-
    `r-eh --
                    \eB
    % -dy
    d-iy) }
    % to
                    \domTupLabel "[a8]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                    %\multiSet
                    \onceBracketTextAfterBreak
                    \outerNestedBracket "⟦ab6⟧"
    t-uw(
                    \once \override Score.HorizontalBracket.shorten-pair = #'(-15 . -1.5)
                    \innerNestedBracket "⟦aa2⟧"
    % drop
    `dr-aa-p8) }
    | % Bar 4 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    \domTupLabel "[a9]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        \eB
    % bombs
    @b-aa-mz8 } r8
    % but he
                    \domTupLabel "[b7]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    `b-ah-t16( h-iy
                        \eB
    % keeps
    `k-iy-ps8) }
                    \domTupLabel "[A10]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦AB7⟧"
    % on for-
    @Q-aa-n }
                    \domTupLabel "[B8]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    %  for-
    f-er16( --
    % -get
    `g-eh --
                         \eB
    % -ting}
    t-ih-G16) }
                    \domTupLabel "[E1]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EF1⟧"
    % what he
    `w-ah-t( % \finger \markup { \teeny \smaller \smaller \smaller  "ə"}
    h-iy) % \finger \markup { \teeny \smaller \smaller \smaller "i"}

    % wrote
    `r-ow-t } |

    % Bar 5 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        _16
                      \domTupLabel "[F1]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                      \eB
    % down
    @d-aw-n8 }
                      \domTupLabel "[E2]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                      %\multiSet
                    \innerNestedBracket "⟦EF*2⟧"
    % the
    D-ah16( % \finger \markup { \teeny \smaller \smaller \smaller  "ə"}

    % whole
    `h-ow-l8) }
                    \domTupLabel "[F2]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %{ \eB %}
    % crowd
    @kr-aw-d }
                    \domTupLabel "[E3]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    %{ \innerNestedBracket "⟦EF3⟧" %}
    % goes so
    `g-ow-z }
    % goes so
                    \domTupLabel "[F3]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    s-ow16(
                        \eB
    % loud
    `l-aw-d) }
                        _16
                    \domTupLabel "[E4]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EF3⟧"
    % he
    h-iy( % \finger \markup { \teeny \smaller \smaller \smaller "i"}

    % o-
    `y-ow8) -- } |

    % Bar 6 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    \domTupLabel "[F4]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % -pens his
    p-ih-nz16( % \finger \markup { \teeny \smaller \smaller \smaller "i"}
     h-ih-z % \finger \markup { \teeny \smaller \smaller \smaller "i"}

                        \eB
    % mouth
    `m-aw-T8) }
                    \domTupLabel "[ʌəɚ]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EF*4⟧"
    % but the
    `b-ah-t16(
    D-ah % \finger \markup { \teeny \smaller \smaller \smaller  "ə"}
                    %{ } \domTupLabel "XX" \boldTup
                \smallTup
                \whiteoutTup
                \upTup { %}
    % words
    `w-er-dz8) % % \finger \markup { \teeny \smaller \smaller \smaller "ɝ" }
                    } \domTupLabel "[E5]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % wont
    `w-ow-nt }
                    \domTupLabel "[F5]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    %  come
    k-ah16( % \finger \markup { \teeny \smaller \smaller \smaller  "ə"}
                        \eB
    % out
    `m-aw-t) }
                        r16
                    \domTupLabel "[E6]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EF5⟧"
    % he's
    h-iy-z( % \finger \markup { \teeny \smaller \smaller \smaller "i"}

    % cho-
    `C-ow8)  -- } |

    % Bar 7 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    \domTupLabel "[F6]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % -king
    k-ih-G16( % \finger \markup { \teeny \smaller \smaller \smaller "i"}

                        \eB
    % how
    @h-aw8) } r16

                    \domTupLabel "[XX]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EF*6⟧"
    % eve-ry
    `Q-eh-v16( % \finger \markup { \teeny \smaller \smaller \smaller  "ii"} --
    r-iy) % \finger \markup { \teeny \smaller \smaller \smaller "i"} --
                    } \domTupLabel "[XX]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % bo-dys
    `b-ah( --
    d-iy-z) % \finger \markup { \teeny \smaller \smaller \smaller "i"}
                    } \domTupLabel "[E7]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % jo-
    `J-ow8 --
                    } \domTupLabel "[F7]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % -king
    k-ih-G16( % \finger \markup { \teeny \smaller \smaller \smaller "i"}
                        \eB
    % now
    @n-aw) }
                        r16
                    \domTupLabel "[E8]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EF7⟧"
    % the
    D-ah( % \finger \markup { \teeny \smaller \smaller \smaller  "ə"}
    % clocks
    `kl-aa-ks8) }
    | % Bar 8 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    \domTupLabel "[F8]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % run
    r-ah-n16( % \finger \markup { \teeny \smaller \smaller \smaller  "ə"}
                        \eB
    % out
    @Q-aw-t8) } r16

                    \domTupLabel "[E9]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EF*8⟧"
    % times up
    `t-ay-mz8(
                    %{ } \domTupLabel "XX" \boldTup
                \smallTup
                \whiteoutTup
                \upTup { %}
    %  up
    Q-ah-p) % \finger \markup { \teeny \smaller \smaller \smaller  "ə"}
                    %{ } \domTupLabel "XX" \boldTup
                \smallTup
                \whiteoutTup
                \upTup { %}
    % o-
    `Q-ow --
                    } \domTupLabel "[F9]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % ver blaow
    v-er16( % \finger \markup { \teeny \smaller \smaller \smaller  "ɚ"}

                        \eB
    % ver blaow
    @bl-aw16)
                      } _16 r16
                    \domTupLabel "[g1]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % snap
    `sn-ae-p8 }
    | % Bar 9 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    \domTupLabel "[G*2]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "[G*GE1]"
    % back to re-
    @b-ae-k8( t-uw16 r-iy) -- }
                    \domTupLabel "[G3]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % -a-li-ty
    `y-ae( l-ih -- t-iy) }
                    \domTupLabel "[E*10]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        \eB
    % oh
    @Q-ow }
                        r16
                    \domTupLabel "[E11]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EGE2⟧"
    % there
    D-eh-r16( % \finger \markup { \teeny \smaller \smaller \smaller  "ii"}

    % goes
    `g-ow-z8) }
                    \domTupLabel "[G4]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % gra-vi-ty
    `gr-ae16( -- v-ih -- t-iy) }
                    \domTupLabel "[E*12]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        \eB
    % oh
    @Q-ow }
    | % Bar 10 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        r16
                    \domTupLabel "[E13]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EGE3⟧"
    % there
    D-eh-r16( % \finger \markup { \teeny \smaller \smaller \smaller  "ii"}
    % goes
    `g-ow-z8) }
                    \domTupLabel "[G5]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % rab-bit he
    `r-ae16( -- b-ih-t h-iy) }
                    \domTupLabel "⟦E*14⟧" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        \eB
    % choked
    @C-ow-kt }
                        r16
                    \domTupLabel "[E15]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EGE4⟧"
    % hes
    h-iy-z16( % \finger \markup { \teeny \smaller \smaller \smaller "i"}

    % so
    `s-ow8) }
                    \domTupLabel "[G*6]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % mad but he
    `m-ae-d16( b-ah-t h-iy) }
                    \domTupLabel "[E*16]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        \eB
    % wont
    @w-ow-nt }
    | % Bar 11 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        _16
                    \domTupLabel "[E17]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EGE5⟧"
    % give
    g-ih16( % \finger \markup { \teeny \smaller \smaller \smaller "i"}

    % up
    `<>v-uh-p8) }
                    \domTupLabel "[G7]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % that ea-sy
    `D-ae-t16( Q-iy -- z-iy) }
                    \domTupLabel "[E*18]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        \eB
    % no
    @n-ow }
                        r16
                    \domTupLabel "[E19]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EGE6⟧"
    % he
    h-iy( % \finger \markup { \teeny \smaller \smaller \smaller "i"}

    % wont
    `w-ow-nt8) }
                    \domTupLabel "[G8]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % have it he
    `h-ae16( v-ih-t h-iy) }
                    \domTupLabel "[E*20]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        \eB
    % knows
    @n-ow-z }
    | % Bar 12 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        _16
                    \domTupLabel "[E21]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EGE7⟧"
    % hes
    h-ih-z( % \finger \markup { \teeny \smaller \smaller \smaller "i"}

    % whole
    `h-ow-l8) }
                    \domTupLabel "[G*9]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % backs to these
    `b-ae-ks16( t-ah D-iy-z) }
                    \domTupLabel "[E*22]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        \eB
    % ropes
    @r-ow-ps }
                        r16
                    \domTupLabel "[E23]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EGE8⟧"
    % it
    Q-ih-t( % \finger \markup { \teeny \smaller \smaller \smaller "i"}

    % dont
    `d-ow-nt8) }
                    \domTupLabel "[G*10]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % mat-ter hes
    `m-ae16( -- t-er h-iy-z) }
                    \domTupLabel "[E*24]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        \eB
    % dope
    @d-ow-p }
    | % Bar 13 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        r16
                    \domTupLabel "[E25]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EGE9⟧"
    % he
    h-iy( % \finger \markup { \teeny \smaller \smaller \smaller "i"}

    % knows
    `n-ow-z8) }
                    \domTupLabel "[G*11]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % that but hes
    `D-ae-t16( b-ah-t h-iy-z) }
                    \domTupLabel "[E*26]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        \eB
    % broke
    @br-ow-k }
                        r16
                    \domTupLabel "[E27]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EGE10⟧"
    % hes
    h-iy-z( % \finger \markup { \teeny \smaller \smaller \smaller "i"}

    % so
    `s-ow8) }
                    \domTupLabel "[G12]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % stag-nant he
    `st-ae-g16( -- n-ih-nt h-iy) }
                    \domTupLabel "[E*28]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        \eB
    % knows
    @n-ow-z }
    | % Bar 14 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    _16
                    \domTupLabel "[E29]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                    %\multiSet
                    \innerNestedBracket "⟦EGE11⟧"
    % when he
    w-eh-n32( % \finger \markup { \teeny \smaller \smaller \smaller  "ii"}
    h-iy % \finger \markup { \teeny \smaller \smaller \smaller "i"}

    % goes
    `g-ow-z8) }

                    \domTupLabel "[G*13]" \boldTup
                \smallTup
                \whiteoutTup \muchLowerTup
                \upTup {
                  \tweak TupletBracket.padding #2
                        \tuplet 3/2 8 {
    % back to
    `b-ae-k8( t-ah16 }
    % this -
    D-ih-s16) }
                    \domTupLabel "[E*30]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        \eB
    %  mo-
    @m-ow --
        _16 }
                    \domTupLabel "[E31]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EGE12⟧"
    % -bile
    b-el( % \finger \markup { \teeny \smaller \smaller \smaller  "ɫ̩"}
    % home
    `h-ow-m8) }

                    \domTupLabel "[G14]" \boldTup
                \smallTup
                \whiteoutTup \muchLowerTup
                \upTup {
                  \tweak TupletBracket.padding #2
                        \tuplet 3/2 8 {
    % thats when
    `D-ae-ts8( w-eh-n16 }
                        \eB
    % its
    Q-ih-ts16) } r16
    | % Bar 15 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    \domTupLabel "[G*15]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦G*EG13⟧"
    % back to the
    @b-ae-k8( t-uw16 D-ah) }
                    \domTupLabel "[G*16]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % lab a-gain
    `l-ae( b-ah -- g-ih-n) }
                    \domTupLabel "[E*32]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        \eB
    % yo
    @y-ow }
                        r16
                    \domTupLabel "[E33]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦EGE14⟧"
    % This
    D-ih-s( % \finger \markup { \teeny \smaller \smaller \smaller "i"}

    % whole
    `h-ow-l8)
                  }  \domTupLabel "[G17]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % rhap-so-
    `r-ae-p16( -- s-ih --
                        \eB
    % -dy
    d-iy) }
                    \domTupLabel "[e34]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %\multiSet
                    \innerNestedBracket "⟦ege15⟧"
    % bet-
    `b-eh( % \finger \markup { \teeny \smaller \smaller \smaller  "ii"} --
    | % Bar 16 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % -ter goe
    t-er % \finger \markup { \teeny \smaller \smaller \smaller  "ɚ"}
    g-ow) }
                    \domTupLabel "[g*18]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % cap-ture
    `k-ae-p( -- C-er
    % This
    D-ih-s) }
                    \domTupLabel "[e35]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                        %{ \eB %}
    % mo-
    `m-ow( --
                    \eb    %\multiSet
    % - ment
    m-eh-nt) % \finger \markup { \teeny \smaller \smaller \smaller  "ii"}
                    } \domTupLabel "[e36]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
                    \innerNestedBracket "⟦E*EG16⟧"
    % and
    Q-en( % \finger \markup { \teeny \smaller \smaller \smaller  "n̩"}
    % hope
    `h-ow) }
                    \domTupLabel "[e37]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    %  it
    p-ih-t( % \finger \markup { \teeny \smaller \smaller \smaller "i"}

    % dont
    `d-ow-nt8) }
                    \domTupLabel "[G*19]" \boldTup
                \smallTup
                \whiteoutTup
                \upTup {
    % pass
    @p-ae-s16(
                        \eB
    % him
    h-ih-m) } r8  |
      }


%{ #(set-global-staff-size 20) %}
%{ \justP %}
\lyricNotation \songLyricsDomains \songSylsNew
%{ \pageBreak %}
%{ #(set-global-staff-size 15) %}
%{ \oneLineLyricNotation \songLyricsRainbow \songSylsPlain %}
