\version "2.21.80"

% Listen to event properties created by lyricNotationInput and add
% stress and accent articulations appropriately.

stressSymbol = \markup \fontsize #7 "´"

stressEvent = ^\stressSymbol
accentEvent = ^>

#(define ((thing-adder recognized-event-property-name thing) music)
   (let loop ((m music))
     (if (not (eq? m '()))
       (let* ((is-note (music-is-of-type? m 'note-event))
              (is-chord (music-is-of-type? m 'event-chord)))
          (if (or is-note is-chord)
              (if (eq? (ly:music-property m recognized-event-property-name) #t)
                  (let ((relevant-property-to-set (if is-note 'articulations 'elements)))
                     (ly:music-set-property! m
                                             relevant-property-to-set
                                             (cons thing (ly:music-property m relevant-property-to-set)))))
              (begin
               (loop (ly:music-property m 'element))
               (for-each loop (ly:music-property m 'elements))))))))

stressAdder = #(thing-adder 'is-stressed stressEvent)
accentAdder = #(thing-adder 'is-accented accentEvent)

addStressAndAccent =
#(define-music-function (music) (ly:music?)
     (stressAdder music)
     (accentAdder music)
     music)

%{
\include "input.ily"
\addStressAndAccent
\lyricNotationInput
\lyricmode { n-UWR-n1 @f-EYR8 `n-UWR `@f-AA }
%}