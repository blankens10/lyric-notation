\version "2.22.0"

#(use-modules (srfi srfi-26)
              (ice-9 receive))

% Align scripts above stems horizontally, not
% above note columns. Add beams (or rests), if
% any, as support elements.

% FIXME: What's the point of delay-stem-support?! It looks unused.

#(define (add-support! stem scripts)
   (for-each
     (lambda (script)
       (ly:grob-set-parent! script X stem)
       (ly:pointer-group-interface::add-grob stem
                                             'script-children
                                             script))
     scripts))

#(define (add-support-now? script)
   (null? (ly:grob-property script 'delay-stem-support)))

#(define (Align_scripts_engraver context)
   (let ((scripts '())
         (stem #f)
         (last-stem #f))
     (make-engraver
       (acknowledgers
         ((text-script-interface engraver grob source-engraver)
            (set! scripts (cons grob scripts)))
         ((script-interface engraver grob source-engraver)
            (set! scripts (cons grob scripts)))
         ((stem-interface engraver grob source-engraver)
            (set! stem grob)))
       ((stop-translation-timestep engraver)
          (receive (now later)
            (partition! add-support-now? scripts)
            (if stem
                (add-support! stem now))
            (if last-stem
                (add-support! last-stem later)))
          (set! scripts '())
          (set! last-stem stem)
          (set! stem #f)))))

#(define (add-support-for-beam stem)
   (let* ((script-array (ly:grob-object stem 'script-children))
          (scripts (if (ly:grob-array? script-array)
                       (ly:grob-array->list script-array)
                       '()))
          (beam (ly:grob-object stem 'beam))
          (rest (ly:grob-object stem 'rest))
          (support (cond ((not (null? beam))
                          beam)
                         ((not (null? rest))
                          rest)
                         (else
                          stem))))
     (for-each
       (cute ly:pointer-group-interface::add-grob
             <>
             'side-support-elements
             support)
       scripts)))


\layout {
  \context {
    \Score
    \consists #Align_scripts_engraver
    \override Stem.before-line-breaking = #add-support-for-beam
  }
}