\version "2.21.80"

% Without this, the alignment of articulations is off as
% centered note heads disturb it.

#(define (get-script-note-head grob)
   (ly:grob-parent grob X))

#(define (get-text-script-note-head grob)
   (let* ((note-column (ly:grob-parent grob X))
          (note-heads (ly:grob-array->list (ly:grob-object note-column 'note-heads))))
      (first note-heads)))


#(define ((articulation-calc-after-line-breaking property-name note-head-getter) grob)
   (let* ((one-note-head (false-if-exception (note-head-getter grob))))
      (if one-note-head
          (ly:grob-set-parent! grob
                               X
                               (ly:grob-object one-note-head 'stem))
          (ly:warning "Can't find note head associated to articulation")))
   (ly:output-def-lookup (ly:grob-layout grob) property-name))

\paper {
  stress-alignment = 0
  accent-alignment = 0
}

\layout {
  \context {
    \Voice
    \override Script.self-alignment-X = #(articulation-calc-after-line-breaking 'accent-alignment get-text-script-note-head)
    \override TextScript.self-alignment-X = #(articulation-calc-after-line-breaking 'stress-alignment get-script-note-head)
  }
}