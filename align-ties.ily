\version "2.22.0"

% Apparently, there is no way to set a grob property
% on the fly. Thus, we use a special marker property.
% The callback in before-line-breaking is evaluated
% early, thus this should be fine.
#(set-object-property! 'is-fully-on-same-beam 'backend-type? boolean?)
#(set-object-property! 'beam-compensation-offset 'backend-type? number?)

#(define (get-maybe-stem-beam maybe-stem)
   (if (null? maybe-stem)
       '()
       (ly:grob-object maybe-stem 'beam)))

#(define (set-tie-alignment tie)
   (let* ((left-head (ly:spanner-bound tie LEFT))
          (right-head (ly:spanner-bound tie RIGHT))
          (left-stem (ly:grob-object left-head 'stem))
          (right-stem (ly:grob-object right-head 'stem))
          (left-beam (get-maybe-stem-beam left-stem))
          (right-beam (get-maybe-stem-beam right-stem)))
     (if (and (ly:event-property (event-cause left-head)
                                'is-phantom)
              (not (null? left-beam))
              (not (null? right-beam))
              (eq? left-beam right-beam))
         (begin
           (ly:grob-set-property! tie 'is-fully-on-same-beam #t)
           (ly:pointer-group-interface::add-grob tie
                                                 'side-support-elements
                                                 left-beam)))))

#(define (make-tie-offset-callback pure)
   (lambda (tie . rest)
     (if (ly:grob-property tie 'is-fully-on-same-beam)
         (apply
           (if pure
               ly:side-position-interface::pure-y-aligned-side
               ly:side-position-interface::y-aligned-side)
           tie
           rest)
         (ly:grob-property tie 'beam-compensation-offset))))



#(define calc-tie-Y-offset
   (ly:make-unpure-pure-container
     (make-tie-offset-callback #f)
     (make-tie-offset-callback #t)))

\layout {
  \context {
    \Voice
    \override Tie.before-line-breaking = #set-tie-alignment
    \override Tie.Y-offset = #calc-tie-Y-offset
    % TODO: should relate to the height of beam segments?
    \override Tie.beam-compensation-offset = 0.1
    \override Tie.is-fully-on-same-beam = ##f
    \override Tie.padding = 0.25
  }
}