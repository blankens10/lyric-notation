\version "2.22.0"

#(define (Break_at_every_barline_engraver context)
   (let ((break-allowed #f)
         ; We start with an empty bar line (for
         ; bar numbers).
         (bar -1)
         (partial-examined #f))
     (make-engraver
       (acknowledgers
         ((bar-line-interface engraver grob source-engraver)
            (set! break-allowed #t)
            (set! bar (1+ bar))))
       ((stop-translation-timestep translator)
          (if (not partial-examined)
              (begin
                (set! partial-examined #t)
                (if (negative?
                      (ly:moment-main
                        (ly:context-property context 'measurePosition)))
                    (set! bar (1- bar)))))
          (if break-allowed
            (let* ((column-grob (ly:context-property context 'currentCommandColumn))
                   (layout (ly:grob-layout column-grob))
                   (one-line (ly:output-def-lookup layout 'one-line-staff))
                   (bars-per-page (ly:output-def-lookup layout (if one-line
                                                                   'bars-per-page-one-line
                                                                   'bars-per-page))))

              (ly:grob-set-property! column-grob 'line-break-permission 'force)
              (ly:grob-set-property! column-grob
                                     'page-break-permission
                                     (if (and
                                           ; Don't break just after pickup.
                                           (not (zero? bar))
                                           (zero? (modulo bar bars-per-page)))
                                         'force
                                         ; Forbid.
                                         '()))))
          (set! break-allowed #f)))))

\layout {
  bars-per-page = 4
  bars-per-page-one-line = 16
  \context {
    \Score
    \consists #Break_at_every_barline_engraver
  }
}


%{
{ c'4 4 4 4 4 4 4 4 4 4 4 4 4 }
%}
