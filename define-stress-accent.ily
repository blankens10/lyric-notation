\version "2.22.0"

\include "define-event.ily"


#(define-event-class 'stress-event 'music-event)

#(define-event!
   'StressEvent
   '((description . "Used to signal stress in a way that causes lyrics to be italicized.")
     (types . (stress-event event))))


#(define-event-class 'accent-event 'music-event)

#(define-event!
   'AccentEvent
   '((description . "Used to signal stress in a way that causes lyrics to be italicized.")
     (types . (accent-event event))))


#(set-object-property! 'stressMarkup 'translation-type? markup?)
#(set-object-property! 'accentMarkup 'translation-type? markup?)


\layout {
  \context {
    \Score
    %{ stressMarkup = \markup \musicglyph "scripts.ustaccatissio" %}
    %{ accentMarkup = \markup \musicglyph "scripts.sforzato" %}
    stressMarkup = \markup \rotate #-20 \fontsize #3.75 "′"
    accentMarkup = \markup \fontsize #0.6 ">"
    \override TextScript.layer = #4
    \override TextScript.padding = #0.5
    %{ \override Script.staff-padding = #'6 %}
    %{ \override Script.outside-staff-padding = #0.2 %}
    %{ \override Script.outside-staff-priority = ##f %}
  }
}
