\version "2.22.0"


#(use-modules (srfi srfi-26))


#(define (greyscale value)
   (let ((upscaled (/ value 255)))
      (rgb-color upscaled upscaled upscaled)))

#(define (calc-staff-symbol-thickness-list grob)
   (let* ((layout (ly:grob-layout grob))
          (setting (ly:output-def-lookup layout 'one-line-staff)))
     (if setting
         '(1)
         (ly:grob-property grob 'default-thickness-list))))

#(define (calc-staff-symbol-line-count grob)
   (length (ly:grob-property grob 'thickness-list)))

#(define (calc-staff-symbol-scaled-thicknesses grob)
   (map
     (cute * <> (ly:staff-symbol-line-thickness grob))
     (ly:grob-property grob 'thickness-list)))

#(define (line-color i height-count interval)
   (greyscale
     (if (zero? height-count)
         ; One-line setting.
         0
         (interval-index
           interval
           ( - 1 (* 2 (/ i height-count)))))))

#(define (calc-staff-symbol-stencil grob)
   (let* ((staff-space (ly:staff-symbol-staff-space grob))
          (thicknesses (ly:grob-property grob 'scaled-thicknesses))
          (line-count (length thicknesses))
          (height-count (- line-count 1))
          (line-positions
            (list-tabulate line-count
                           (lambda (i)
                             (* staff-space
                                (- height-count (* 2 i))))))
          (x-extent (ly:stencil-extent (ly:staff-symbol::print grob)
                                       X))
          (grey-interval (ly:grob-property grob 'grey-interval)))
     (apply ly:stencil-add
       (map
         (lambda (i pos thickness)
            (ly:stencil-translate-axis
               (grob-interpret-markup grob
                #{
                  \markup
                  \with-color #(line-color i height-count grey-interval)
                  \filled-box #x-extent
                              #(cons (- thickness) thickness)
                              #(ly:grob-property grob 'staff-line-blot)
                #})
                (/ pos 2)
                Y))
         (iota line-count)
         line-positions
         thicknesses))))


#(define (calc-staff-symbol-y-extent grob)
   (let* ((thicknesses (ly:grob-property grob 'scaled-thicknesses))
          (height (1- (length thicknesses)))
          (height (* height (ly:staff-symbol-staff-space grob)))
          (height (+ height
                     (+ (first thicknesses)
                        (last thicknesses))))
          (half-height (/ height 2)))
     (cons (- half-height)
           half-height)))

% The default callback uses ly:staff-symbol::height directly...

#(define (calc-bar-line-bar-extent grob)
   (let ((staff-symbol (ly:grob-object grob 'staff-symbol)))
     (ly:grob-extent staff-symbol staff-symbol Y)))

#(set-object-property! 'default-thickness-list 'backend-type? number-list?)
#(set-object-property! 'thickness-list 'backend-type? number-list?)
#(set-object-property! 'inter-line-gap 'backend-type? number?)
#(set-object-property! 'staff-line-blot 'backend-type? number?)
#(set-object-property! 'grey-interval 'backend-type? number-pair?)

\layout {
  \context {
    \Staff
    \override StaffSymbol.default-thickness-list = #'(3 2.5 2 1.67 1.33 1 1 1.33 1.67 2 2.5 3)
    \override StaffSymbol.thickness-list = #calc-staff-symbol-thickness-list
    \override StaffSymbol.staff-line-blot = 0.45
    \override StaffSymbol.grey-interval = #'(0 . 150)
    \override StaffSymbol.stencil = #calc-staff-symbol-stencil
    \override StaffSymbol.scaled-thicknesses = #calc-staff-symbol-scaled-thicknesses
    \override StaffSymbol.line-count = #calc-staff-symbol-line-count
    \override StaffSymbol.Y-extent = #(ly:make-unpure-pure-container
                                        calc-staff-symbol-y-extent)
    \override BarLine.bar-extent = #calc-bar-line-bar-extent
  }
}
