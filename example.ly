\version "2.21.80"

\include "lyric-notation.ily"

%{
I expanded the exmaple to show many different contexts,
and noticed the following issues. I will mark them
as they get fixed:

0. at one point, I changed the staff spacing from 2 to 1.5
    to match the size of the noteheads, but I'm afraid
    that 1.5 is too small to read comfortably.
    Usually the notation should print in one of 2 ways:
    A. Standard layout should be 4 measures (systems) per bar
    B. Alt layout adds a new first system with a pickup bar
    It would be very nice to be able to easily change how many bars
    are shown per page and automatically scale the notation to fit,
    but for now 4 bars a page, or pickup + 4 bars.
1. first system not justified across page, begins in the middle.
    should begin at left margin and go to right margin
2. There is some strange discontinuity
    with the stems on some diphthong notes
3. The alignment of diphthong noteheads is off
    Vowel list example shows first notes with
    pointy red onset headphones
  A* alt note dipthohngs like "there" "chase"
	noteheads are flipped, 2nd to the left of 1st
  B* other dipthongs like "Boy" "go" "Pri"
    both noteheads are centered on stem
  *** I tried to figure out how to fix this,
    but since the last big change, I couldnt find that setting
4. FIXED
5. FIXED
6. FIXED
7. Headphones are in wrong order on "Pri"
	Should be pointy-red first, round-blue second
8. Bold and Italic in lyrics is very strange
	* "Boy" is italic, not just bold
	* "go" is italic/bold, should be neither
	* "chase Pri" is also italic/bold
9. "J" throws an error, but "C" doesn't
	But they seem otherwise identical to me. Strange
10. Still needs vowel clef to identify the spaces,
	which I was thinking could take the form of an instrument name

Re: \alt noteheads
	I don't think the diamonds having circles around them is that big a problem. It might not be ideal, but it's not a priority right now.
The only real issue I have with it is how it looks when combined with the rhotic symbol. But I don't have a good idea of what to do about it.
One possible alternative to using a diamond notehead would be to print a small delta character or triangle just above an otherwise normal notehead
(or some other symbol, such as asterisk). If that's significantly easier, maybe we should try it.
%}

% Example
%{ \lyricNotation
  \lyricmode {
    `b-OY8
    g-OW16
    \alt Q-AH-p
    Q-AH-p \break
    D-EYR-r8
    \alt @D-EYR-r8
    Q-AE-nd16
    \alt C-EY-s
    pr-AY16.
    v-AE-t32
    %"J" throws an error where "C" does not
%    `J-AH-J8
  }
  \lyricmode {
    Boy
    go
    up \break
    there
    and
    chase
    Pri -- vate %}
%    Judge
   %}

% Vowel list example
\lyricNotation
  \lyricmode {
    `Q-IY8
    t-IYR-r
    @Q-IH
    t-EY
    `Q-EH
    t-EYR-r
    @Q-AE
    Q-AH
    `Q-AX
    t-AY
    @Q-AA
    Q-AAR-r
    `t-AW
    Q-ER-r
    @Q-AXR
    t-OY
    %{ `Q-AOR-r
    Q-AO
    @t-OW
    Q-UH
    `t-UWR-r
    Q-UW %}
  }
  \lyricmode {
    `IY
    IYR
    @IH
    EY
    `EH
    EYR
    @AE
    AH
    `AX
    AY
    @AA
    AAR
    `AW
    ER
    @AXR
    OY
      %{ `AOR
      AO
      @OW
      UH
      `UWR
      UW %}
  } %}

% Note this behavior -- is it fine?
% \lyricNotation \lyricmode { \alt r-AY } \lyricmode { ri }
