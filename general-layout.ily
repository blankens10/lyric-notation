\version "2.22.0"


\layout {
  one-line-staff = ##f
  \context {
    \Score
    \remove System_start_delimiter_engraver


  }
  \context {
    \Staff
    \remove Clef_engraver
    \remove Time_signature_engraver
  }
  \context {
    \Voice
    melismaBusyProperties = #'(tieMelismaBusy)
    \override NoteHead.no-ledgers = ##t
    \override NoteHead.stem-attachment = #'(0 . 0)
    \override Tie.direction = #UP
    \override Tie.thickness = 3.0
  %%  Leave above here

    \override TextScript.self-alignment-X = #CENTER
    \override TextScript.font-family = #'sans
    %{ \override TextScript.self-alignment-Y = #DOWN %}
    %{ \override TextScript.staff-padding = 0.3 %}
    %{ \override TextScript.outside-staff-padding = 0.3 %}
    \override Slur.avoid-slur = #'inside
    \override PhrasingSlur.avoid-slur = #'inside
    %{ \override TextScript.outside-staff-padding = 0.3 %}
    \override TextScript.layer = #4
    \override TextScript.outside-staff-priority = ##f
    \slurDown
    \phrasingSlurUp
  }
  \context {
    \Lyrics
    \override VerticalAxisGroup.nonstaff-relatedstaff-spacing =
    #'((basic-distance . 0)
       (minimum-distance . 0)
       (padding . 0.15)
       (staff-padding . 0.2)
       (stretchability . 50))
    \override LyricText.font-size = -1
  }
}
