\version "2.22.0"

#(use-modules (srfi srfi-26))

#(for-each
   (cute set-object-property! <> 'backend-type? number?)
   '(headphone-contour-thickness
     headphone-half-width
     diamond-half-width
     diamond-outline-thickness
     headphone-half-height
     headphone-blot
     headphone-translation-amount
     headphone-pointiness
     glottal-translation-amount
     glottal-pointiness
     headphone-stop-at))

\layout {
  \override NoteHead.headphone-contour-thickness = 0.1
  \override NoteHead.headphone-half-width = 0.5
  \override NoteHead.diamond-half-width = 0.49
  \override NoteHead.diamond-outline-thickness = 0.10
  \override NoteHead.headphone-half-height = 0.5
  \override NoteHead.headphone-blot = 0.9
  \override NoteHead.headphone-translation-amount = 0.65
  \override NoteHead.headphone-pointiness = 0.45
  \override NoteHead.glottal-translation-amount = 0.45
  \override NoteHead.glottal-pointiness = 0
  \override NoteHead.headphone-stop-at = 0.2
}


% All functions take a grob parameter and return a stencil.
% They are used to write NoteHead callbacks.

#(define (black-whiteout grob arg)
   (stencil-whiteout-outline
      (grob-interpret-markup grob arg)
      (ly:grob-property grob 'headphone-contour-thickness)
      black))

#(define (box-with-two-corners-rounded grob x-extent y-extent blot)
   (let* ((x-start (car x-extent))
          (x-end (cdr x-extent))
          (x-length (- x-end x-start))
          (rectangle-x-extent (cons (- x-end (/ (min x-length blot) 2)) x-end)))
   (grob-interpret-markup grob
     (make-combine-markup
       (make-filled-box-markup x-extent y-extent blot)
       (make-filled-box-markup rectangle-x-extent y-extent 0)))))


% TODO: This is currently defined using a rounded box.
% However, this doesn't play well with diamond note heads
% because those are rotated squares, with same width and
% height, which means that unless the regular note head
% is completely circular, the headphones can't fit well
% around the diamond. It should be defined using a path,
% but this will probably break much of the rest of the
% code.

#(define (note-head-base grob)
   (let ((half-width (ly:grob-property grob 'headphone-half-width))
         (half-heigth (ly:grob-property grob 'headphone-half-height))
         (blot (ly:grob-property grob 'headphone-blot)))
     (grob-interpret-markup grob
       (make-filled-box-markup
         (cons (- half-width) half-width)
         (cons (- half-heigth) half-heigth)
         blot))))

#(define (note-head-expanded grob)
   (black-whiteout grob
                   (make-stencil-markup
                     (note-head-base grob))))


#(define (tilted-square grob half-width)
   (let ((square-extent (cons (- half-width)
                              half-width)))
     (make-rotate-markup 45
                         (make-filled-box-markup
                           square-extent
                           square-extent
                           ; No blot.
                           0))))

#(define (diamond-note-head grob)
   (let* ((half-width (ly:grob-property grob 'diamond-half-width))
          (outline-thickness (ly:grob-property grob 'diamond-outline-thickness)))
     (grob-interpret-markup grob
       (make-combine-markup
          (make-with-color-markup white
                                  (tilted-square grob (+ half-width outline-thickness)))
          (tilted-square grob half-width)))))

#(define (regular-headphone grob color)
  (let ((half-width (ly:grob-property grob 'headphone-half-width))
        (half-height (ly:grob-property grob 'headphone-half-height))
        (translation-amount (ly:grob-property grob 'headphone-translation-amount))
        (blot (ly:grob-property grob 'headphone-blot)))
    (stencil-with-color
      (black-whiteout grob
                      (make-filled-box-markup
                        (cons (- half-width) (+ half-width translation-amount))
                        (cons (- half-height) half-height)
                        blot))
      color)))


#(define (pointy-headphone-helper grob color pointiness translation-amount)

   (define (square x) (expt x 2))

   (let* ((half-width (ly:grob-property grob 'headphone-half-width))
          (half-height (ly:grob-property grob 'headphone-half-height))
          (negative-half-height (- half-height))
          (contour-thickness (ly:grob-property grob 'headphone-contour-thickness))
          (x-endpoint (+ half-width translation-amount))
          (x-startpoint (- x-endpoint pointiness))
          ; A bit of trigonometry (hopefully correct).
          (delta-X (/ (* contour-thickness (sqrt (+ (square pointiness) (square half-height)))) half-height))
          (augmented-x-endpoint (+ x-endpoint delta-X))
          (augmented-x-startpoint (+ x-startpoint (- delta-X (sqrt (- (square delta-X) (square contour-thickness))))))
          (augmented-y-bottom (- (- half-height) contour-thickness))
          (augmented-y-top (+ half-height contour-thickness))
          (pointy-shape
            (make-path-markup 0
                              `((moveto ,x-startpoint ,half-height)
                                 (lineto ,x-endpoint 0)
                                 (lineto ,x-startpoint ,negative-half-height)
                                 (lineto 0 ,negative-half-height)
                                 (lineto 0 ,half-height)
                                 (closepath))))
          (augmented-pointy-shape
            (make-path-markup 0
                              `((moveto ,augmented-x-startpoint ,augmented-y-bottom)
                                (lineto ,augmented-x-endpoint 0)
                                (lineto ,augmented-x-startpoint ,augmented-y-top)
                                (lineto 0 ,augmented-y-top)
                                (lineto 0 ,augmented-y-bottom)
                                (closepath)))))
     (grob-interpret-markup grob
       ; Can't use black-whiteout here as it doesn't work well on
       ; angled stencils. So, we draw a black path around the pointy
       ; part manually.
       ;
       ; black-whiteout doesn't take the whiteout into account for
       ; the dimensions. Do the same here to keep note head alignment
       ; code unified.
       (make-with-outline-markup
         pointy-shape
         (make-override-markup
           '(filled . #t)
           (make-overlay-markup
             (list
               augmented-pointy-shape
               (make-with-color-markup color
                                       pointy-shape))))))))


#(define (pointy-headphone grob color)
   (pointy-headphone-helper grob
     color
     (ly:grob-property grob 'headphone-pointiness)
     (ly:grob-property grob 'headphone-translation-amount)))


#(define (glottal-headphone grob color)
   (pointy-headphone-helper grob
     color
     (ly:grob-property grob 'glottal-pointiness)
     (ly:grob-property grob 'glottal-translation-amount)))


#(define (stack-headphones grob base left-headphones right-headphones)
   "Create a note head from headphones to be used on the left and on the right."

   (define (stack headphone-list dir translation-amount)
     (map
       (lambda (i headphone)
         (make-translate-markup
           (cons (* i translation-amount dir) 0)
           (make-scale-markup (cons dir 1)
                              headphone)))
       (reverse (iota (length headphone-list)))
       (reverse headphone-list)))

   (let ((translation-amount (ly:grob-property grob 'headphone-translation-amount)))
     (grob-interpret-markup grob
       (make-overlay-markup
         (append
           (stack (reverse left-headphones) -1 translation-amount)
           (stack right-headphones 1 translation-amount)
           (list base))))))
