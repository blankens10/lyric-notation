\version "2.22.0"


\include "define-event.ily"
\include "define-grob.ily"
\include "color-gradient-path.ily"
%{ \include "highlight-colors" %}

#(use-modules (srfi srfi-26))

#(define-event-class 'highlight-event 'span-event)

#(define-event!
   'HighlightEvent
   '((description . "Used to signal where colored highlights start and stop.")
     (types . (post-event highlight-event event))))

#(set-object-property! 'all-grobs 'backend-type? grob-list?)
#(set-object-property! 'box-x-extra 'backend-type? number-pair?)
#(set-object-property! 'box-y-extra 'backend-type? number-pair?)
#(set-object-property! 'blot 'backend-type? number?)
#(set-object-property! 'contour-thickness 'backend-type? number?)
#(set-object-property! 'outside-highlight 'backend-type? boolean?)
#(set-object-property! 'bounding-musical-columns 'backend-type? pair?)
#(set-object-property! 'color-gradient 'backend-type? boolean?)
#(set-object-property! 'left-color 'backend-type? color?)
#(set-object-property! 'right-color 'backend-type? color?)

#(define ((compare-from-key key-function compare-function) a b)
   (compare-function (key-function a)
                     (key-function b)))

#(define paper-column-when<?
   (compare-from-key
     (cute ly:grob-property <> 'when)
     ly:moment<?))

#(define paper-column-when<=?
   (compare-from-key
     (cute ly:grob-property <> 'when)
     (lambda (mom1 mom2)
       (or (ly:moment<? mom1 mom2)
           (equal? mom1 mom2)))))

#(define (is-musical-grob? grob)
   (null? (ly:grob-property grob 'non-musical)))

#(define (is-paper-column? grob)
   (grob::has-interface grob 'paper-column-interface))

#(define (is-musical-paper-column? grob)
   (and (is-paper-column? grob)
        (is-musical-grob? grob)))

#(define (inside-highlight? grob)
   (null? (ly:grob-property grob 'outside-highlight)))

#(define (get-broken-pieces grobs elements-list)
   ; TODO: quadratic behavior! Where is a sensible data
   ; structure in Guile that can prevent this?
   (filter
     (lambda (g)
       (memq (ly:grob-original g)
             grobs))
       elements-list))

% TODO: we need the System, so can only do this
% after line breaking. Is this sane?

#(define (highlight::set-columns! grob)
   (let* ((left-bound (ly:spanner-bound grob LEFT))
          (right-bound (ly:spanner-bound grob RIGHT))
          (sys (ly:grob-system grob))
          (all-elements
            (ly:grob-array->list (ly:grob-object sys 'all-elements)))
          (musical-columns-unsorted
            (filter is-musical-paper-column? all-elements))
          (musical-columns
            (sort musical-columns-unsorted
                  paper-column-when<=?))
          (first-musical-column
            (find (cute paper-column-when<=? left-bound <>)
                  musical-columns))
          (last-musical-column
            (find (cute paper-column-when<? <> right-bound)
                  (reverse musical-columns))))
     (if (not (and first-musical-column
                   last-musical-column))
         (ly:grob-suicide! grob)
         (ly:grob-set-property! grob
                                'bounding-musical-columns
                                (cons first-musical-column last-musical-column)))))


#(define (highlight::x-extent grob)
   (let* ((sys (ly:grob-system grob))
          (x-position (ly:grob-relative-coordinate grob sys X))
          (x-extra (ly:grob-property grob 'box-x-extra))
          (bounding-musical-columns (ly:grob-property grob 'bounding-musical-columns)))
     (cons
       (+ (interval-start (ly:grob-extent (car bounding-musical-columns)
                                          sys
                                          X))
          (- x-position)
          (interval-start x-extra))
       (+ (interval-end (ly:grob-extent (cdr bounding-musical-columns)
                                        sys
                                        X))
          (- x-position)
          (interval-end x-extra)))))

#(define (highlight::y-extent grob)
   (let* ((sys (ly:grob-system grob))
          (all-grobs (ly:grob-property grob 'all-grobs))
          (all-elements
            (ly:grob-array->list (ly:grob-object sys 'all-elements)))
          (grobs-to-consider
            (get-broken-pieces
              (filter
                (lambda (g)
                  (and (is-musical-grob? g)
                       (inside-highlight? g)))
                all-grobs)
              all-elements))
          (y-position (ly:grob-relative-coordinate grob sys Y))
          (y-absolute-extent
           (reduce interval-union
                   '(-inf.0 . +inf.0)
                   (map
                     (cute ly:grob-extent <> sys Y)
                     grobs-to-consider)))
          (y-extra (ly:grob-property grob 'box-y-extra)))
     (cons
       (+ (interval-start y-absolute-extent)
          (- y-position)
          (interval-start y-extra))
       (+ (interval-end y-absolute-extent)
          (- y-position)
          (interval-end y-extra)))))

#(define (highlight::markup grob)
   (let* ((x-extent (ly:grob-property grob 'X-extent))
          (x-start (interval-start x-extent))
          (x-end (interval-end x-extent))
          (y-extent (ly:grob-property grob 'Y-extent))
          (y-start (interval-start y-extent))
          (y-end (interval-end y-extent))
          (blot (ly:grob-property grob 'blot))
          (color-gradient (ly:grob-property grob 'color-gradient))
          (left-color (ly:grob-property grob 'left-color))
          (right-color (ly:grob-property grob 'right-color))
          ; TODO: blot is not really a blot then. The problem is that
          ; proper blot is hard to achieve in \markup \path, but we
          ; want the color gradient.
          (path
            `((moveto ,(+ x-start blot) ,y-start)
              (lineto ,(- x-end blot) ,y-start)
              (curveto ,x-end ,y-start ,x-end ,y-start ,x-end ,(+ y-start blot))
              (lineto ,x-end ,(- y-end blot))
              (curveto ,x-end ,y-end ,x-end ,y-end ,(- x-end blot) ,y-end)
              (lineto ,(+ x-start blot) ,y-end)
              (curveto ,x-start ,y-end ,x-start ,y-end ,x-start ,(- y-end blot))
              (lineto ,x-start ,(+ y-start blot))
              (curveto ,x-start ,y-start ,x-start ,y-start ,(+ x-start blot) ,y-start))))
     (if (null? color-gradient)
         ; Leave the job to the default implementation of color.
         (make-path-markup
           0 ; thickness
           path)
         (make-gradient-path-markup left-color right-color path))))


#(define (analysis-number::print grob)
   (let* ((cause (event-cause grob))
          (n (ly:event-property cause 'number))
          (contour-thickness (ly:grob-property grob 'contour-thickness)))
     (if (zero? n)
         empty-stencil
         (stencil-whiteout-outline
           (grob-interpret-markup grob (number->string n))
           contour-thickness
           black))))

#(define-grob!
  'Highlight
  `((after-line-breaking . ,highlight::set-columns!)
    (stencil . ,ly:text-interface::print)
    (text . ,highlight::markup)
    (filled . #t)
    (layer . -10)
    (box-x-extra . (-0.1 . 0.1))
    (box-y-extra . (-0.5 . 0.5))
    (blot . 1.2)
    (X-extent . ,highlight::x-extent)
    (Y-extent . ,highlight::y-extent)
    (meta . ((class . Spanner)
             (interfaces . (highlight-interface
                            text-interface))))))

%{
  Mostly copied from TextScript.

  (outside-highlight is for purism, because engravers don't
  acknowledge their own grobs anyway, so the AnalysisNumber
  is not included in the list of grobs that the Highlight
  encompasses, 'all-grobs.)
%}


#(define-grob!
  'AnalysisNumber
  `((avoid-slur . around)
    (contour-thickness . 0.07)
    (cross-staff . #f)
    (direction . ,UP)
    (extra-spacing-width . (+inf.0 . -inf.0))
    (font-size . -2)
    (outside-highlight . #t)
    (outside-staff-horizontal-padding . 0.2)
    ; (outside-staff-padding . 0.2)
    (outside-staff-priority . 450)
    (padding . 0.3)
    (parent-alignment-X . #f)
    (script-priority . 200)
    (self-alignment-X . ,CENTER)
    (side-axis . ,Y)
    (slur-padding . 0.5)
    (staff-padding . 0.5)
    (stencil . ,analysis-number::print)
    (vertical-skylines . ,grob::unpure-vertical-skylines-from-stencil)
    (Y-extent . ,grob::always-Y-extent-from-stencil)
    (X-align-on-main-noteheads . #t)
    (X-offset . ,ly:self-alignment-interface::aligned-on-x-parent)
    (Y-offset . ,side-position-interface::y-aligned-side)
    (meta . ((class . Item)
             (interfaces . (font-interface
                            instrument-specific-markup-interface
                            outside-staff-interface
                            self-alignment-interface
                            side-position-interface
                            text-interface
                            text-script-interface
                            analysis-number-interface))))))


#(define (add-grobs-to-highlight! grobs highlight)
   (ly:grob-set-property! highlight
                          'all-grobs
                          (append grobs
                                  (ly:grob-property highlight 'all-grobs))))

#(define (set-color! highlight color event id)
   (cond
     ((color? color)
      (ly:grob-set-property! highlight 'color color))
     ((pair? color)
      (ly:grob-set-property! highlight 'color-gradient #t)
      (ly:grob-set-property! highlight 'left-color (car color))
      (ly:grob-set-property! highlight 'right-color (cdr color)))
     (else
      (ly:event-warning event
                        "No color defined for spanner with id '~s' (using black)"
                        id))))

#(define (make-number engraver event highlight)
   ; Copied from the Text_engraver.
   (let ((number (ly:engraver-make-grob engraver 'AnalysisNumber event))
         (n (ly:event-property event 'number))
         (color (ly:grob-property highlight 'color)))
     (ly:grob-set-property! number 'script-priority 199)
     (ly:pointer-group-interface::add-grob number
                                           'side-support-elements
                                           highlight)
     (ly:grob-set-property! number 'color color)
     number))

#(define (set-number-parent! number note-column)
   (if (null? (ly:grob-parent number X))
       (let* ((note-head-array (ly:grob-object note-column 'note-heads))
              (note-heads (if (null? note-head-array)
                              '()
                              (ly:grob-array->list note-head-array)))
              (stem (ly:grob-object note-column 'stem))
              (parent (if (null? stem)
                          (ly:grob-object note-column'rest)
                          stem)))
         (ly:grob-set-parent! number X parent))))


#(define (Highlight_engraver context)
   (let ((highlight-event #f)
         (last-spanner #f)
         (last-id #f)
         (current-spanner #f)
         (current-id #f)
         (current-text-script #f)
         (grobs-found-in-timestep '()))

     (make-engraver
       (listeners
         ((highlight-event engraver event)
            (set! highlight-event event)))

       ((process-music engraver)
          (if highlight-event
            (let* ((id (ly:event-property highlight-event 'id)))
              ; If we are starting a new spanner while we didn't
              ; have one before, last-id is #f and id is non-#f.
              ; If we did have one before, this checks whether it
              ; should be continued (same id) or a new one should
              ; be created and the previous one terminated.
              (if (equal? id last-id)
                  ; Continuing the previous spanner.
                  (set! current-spanner last-spanner)
                  ; Make a new spanner and end the previous one
                  ; if applicable.
                  (let* ((colors (ly:context-property context 'highlightColors))
                         (color (assoc-ref colors id))
                         (non-musical-column (ly:context-property context 'currentCommandColumn)))
                    (set! current-spanner
                          (ly:engraver-make-grob engraver 'Highlight highlight-event))
                    (ly:spanner-set-bound! current-spanner LEFT non-musical-column)
                    (set-color! current-spanner color highlight-event id)))
              (set! current-id id)
              ; It's nice here that engravers don't acknowledge their
              ; own grobs. These numbers are not included in the extent
              ; of the highlight box.
              (set! current-text-script (make-number engraver highlight-event current-spanner))
              (ly:grob-set-object! current-text-script 'highlight current-spanner)))
          (if (and last-spanner
                   (not (eq? current-spanner last-spanner)))
            ; The previous spanner was discontinued. This may
            ; be because we don't have an \rh command in this
            ; time step (current-spanner is #f), or we had to
            ; create a new one due to a different id being passed.
            ; It is time to terminate that spanner.
            (let ((non-musical-column (ly:context-property context 'currentCommandColumn)))
              (ly:spanner-set-bound! last-spanner RIGHT non-musical-column)
              (ly:engraver-announce-end-grob engraver
                                             last-spanner
                                             (or current-spanner
                                                 '())))))


       (acknowledgers
         ((grob-interface engraver grob source-engraver)
            (if (or (grob::has-interface grob 'note-column-interface)
                    (grob::has-interface grob 'text-script-interface)
                    (grob::has-interface grob 'lyric-syllable-interface))
              (prepend! grob grobs-found-in-timestep)))

         ((note-column-interface engraver grob source-engraver)
            (if current-text-script
                (set-number-parent! current-text-script grob))))

       ((stop-translation-timestep engraver)
          (if current-spanner
              (add-grobs-to-highlight! grobs-found-in-timestep current-spanner))
          (set! highlight-event #f)
          (set! grobs-found-in-timestep '())
          (set! last-spanner current-spanner)
          (set! last-id current-id)
          (set! current-spanner #f)
          (set! current-id #f)
          (set! current-text-script #f)))))


rh =
#(define-music-function (id number)
                        (scheme? integer?)
  (make-music 'HighlightEvent
              'id id
              'number number))

#(set-object-property! 'highlightColors 'translation-type? list?)

#(define (make-rhyme-color-setter id color-setting)
   #{
      \context Score
        \applyContext
          #(lambda (context)
             (ly:context-set-property!
               context
               'highlightColors
               (acons
                 id
                 color-setting
                 (ly:context-property context 'highlightColors))))
   #})


rhColor =
#(define-music-function (id color)
                        (scheme? color?)
   (make-rhyme-color-setter id color))

rhColorGradient =
#(define-music-function (id left-color right-color)
                        (scheme? color? color?)
   (make-rhyme-color-setter id (cons left-color right-color)))


\layout {
  \context {
    \Global
    \grobdescriptions #all-grob-descriptions
  }
  \context {
    \Score
    \consists #Highlight_engraver
    highlightColors = #'()
  }
}
%{ \include "highlight-colors" %}
