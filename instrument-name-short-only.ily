\version "2.22.0"

#(define (force-y-spacing grob y-extent arg)
    (let* ((y-centered-arg (markup #:general-align Y CENTER arg))
           (stil (grob-interpret-markup grob y-centered-arg))
           (x (ly:stencil-extent stil X)))
      (ly:stencil-outline stil (make-filled-box-stencil x y-extent))))

#(define (unit-height-right-column grob y-extent args)
   (make-override-markup
     '(baseline-skip . 0)
     (make-right-column-markup
       (map
         (lambda (arg)
           (make-stencil-markup
             (force-y-spacing grob y-extent arg)))
         args))))


#(define (change-instrument-name-text grob)
   (let* ((layout (ly:grob-layout grob))
          (one-line (ly:output-def-lookup layout 'one-line-staff))
          (staff-symbol (ly:grob-object grob 'staff-symbol))
          (staff-symbol-extent (interval-scale
                                 (ly:grob-extent staff-symbol staff-symbol Y)
                                 (ly:staff-symbol-line-thickness staff-symbol)))
          (line-count (ly:grob-property staff-symbol 'line-count))
          (staff-symbol-extent (interval-scale staff-symbol-extent
                                               (/ (1- line-count)
                                                  line-count))))
     (if one-line
         empty-stencil
         (begin
           (ly:grob-set-property! grob
                                  'long-text
                                  (unit-height-right-column grob
                                    staff-symbol-extent
                                    (ly:output-def-lookup layout 'vowel-list)))
           (ly:grob-set-property! grob
                                  'text
                                  (unit-height-right-column grob
                                     staff-symbol-extent
                                    (ly:output-def-lookup layout 'short-vowel-list)))
           (system-start-text::print grob)))))


\layout {
vowel-list = #'("i" "ɪ" "e" "ɛ" "æ" "ʌ" "ɑ" "ɔ" "o" "ʊ" "u")
short-vowel-list = #'("i" "ɪ" "e" "ɛ" "æ" "ʌ" "ɑ" "ɔ" "o" "ʊ" "u")
  \context {
    \Staff
    % Needed to trigger calculations
    instrumentName = ""
    shortInstrumentName = ""
    \override InstrumentName.self-alignment-X = #RIGHT
    \override InstrumentName.stencil = #change-instrument-name-text
    \override InstrumentName.font-size = -8
  }
}
