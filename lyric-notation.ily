\version "2.22.0"

\include "input.ily"
\include "spacing.ily"
\include "general-layout.ily"
\include "break-at-barlines.ily"
\include "print-note-head.ily"
\include "align-note-heads.ily"
\include "draw-staff-symbol.ily"
\include "proportional-notation.ily"
\include "process-stress-accent.ily"
\include "instrument-name.ily"
\include "partial-indent.ily"
\include "bar-numbers.ily"
\include "stems-beams.ily"
\include "phantom-note-head.ily"
\include "shift-rests-dots.ily"
%{ \include "highlight.ily" %}
\include "one-line-staff-pitches.ily"
\include "align-scripts.ily"
\include "align-ties.ily"
\include "remove-extra-dots.ily"
\include "png-resolution.ily"
\include "breathing-sign.ily"
%{ \include "angle-bracket.ily" %}
%{ \include "analysis-bracket.ily" %}
\include "language.ily"
%{ \include "pitch-registers.ily" %}
\include "stress-accent-lyrics.ily"
\include "shrink-lyrics.ily"
%{ \include "bracket-lyric-support.ily" %}
%{ \include "groove-analysis-tools.ily" %}
%{ \include "rhyme-analysis-tools.ily" %}
\include "analysis-tools.ily"

#(define (lyric-notation-helper one-line)
   (define-scheme-function (lyrics phonetic)
                          (ly:music? ly:music?)
     #{
       \score {
         \layout {
           one-line-staff = #one-line
         }
         <<
           \new Voice = "notation" {
             \bar ""
             #(lyric-notation-input one-line phonetic)
           }
           \new Lyrics \lyricsto "notation" {
             #lyrics
           }
         >>
       }
     #}))

lyricNotation = #(lyric-notation-helper #f)
oneLineLyricNotation = #(lyric-notation-helper #t)
