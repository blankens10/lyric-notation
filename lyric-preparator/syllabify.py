import argparse
import glob
import logging
import os
import pickle
import re
import sys

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)


class Language:

    def syllabify(self, word) :
        '''
        Syllabify a word.

        The word is given as a list of phonemes, e.g., ``["B", "AE1", "T"]``.
        That list is split into a list of lists.
        '''

        # This is the returned data structure.
        syllables = []

        # This maintains a list of phonemes between nuclei.
        internuclei = []

        for phoneme in word:
            match = re.match(r"([A-Z.]+)(\d?)", phoneme)
            if match:
                phoneme = match.group(1)
                digit = match.group(2)
                stress = int(digit) if digit else None
            else:
                raise ValueError(f"Invalid phoneme representation: {phoneme}")

            if phoneme in self.vowels:
                # Split the consonants seen since the last nucleus into coda and onset.

                coda = None
                onset = None

                # If there is a period in the input, split there.
                if "." in internuclei :
                    period = internuclei.index(".")
                    coda = internuclei[:period]
                    onset = internuclei[period+1:]

                else:
                    # Make the largest onset we can. The 'split' variable marks the break point.
                    for split in range(0, len(internuclei)+1) :
                        coda = internuclei[:split]
                        onset = internuclei[split:]

                        # If we are looking at a valid onset, or if we're at the start of the word
                        # (in which case an invalid onset is better than a coda that doesn't follow
                        # a nucleus), or if we've gone through all of the onsets and we didn't find
                        # any that are valid, then split the nonvowels we've seen at this location.
                        if onset in self.onsets or not syllables or not onset:
                            break

                # Tack the coda onto the coda of the last syllable. Can't do it if this
                # is the first syllable.
                if syllables:
                    syllables[-1][3].extend(coda)

                # Make a new syllable out of the onset and nucleus.
                syllables.append((stress, onset, phoneme, []))

                # At this point we've processed the internuclei list.
                internuclei = []

            elif phoneme not in self.consonants and phoneme != ".":
                raise ValueError("Invalid phoneme: " + phoneme)

            else: # a consonant or period
                internuclei.append(phoneme)

        # Done looping through phonemes. We may have consonants left at the end.
        # We may have even not found a nucleus.
        if internuclei:
            if not syllables:
                syllables.append((-1, internuclei, [], []))
            else:
                syllables[-1][3].extend(internuclei)

        return syllables


consonants_en_2l_to_1l_arpabet = {
    "Q": "Q",
    "P": "p",
    "T": "t",
    "K": "k",
    "B": "b",
    "D": "d",
    "G": "g",
    "CH": "C",
    "JH": "J",
    "F": "f",
    "TH": "T",
    "S": "s",
    "SH": "S",
    "HH": "h",
    "V": "v",
    "DH": "D",
    "Z": "z",
    "ZH": "Z",
    "XH": "x",
    "M": "m",
    "N": "n",
    "NG": "G",
    "L": "l",
    "R": "r",
    "Y": "y",
    "W": "w",
    "EL": "L",
    "EN": "N",
    "EM": "M",
}

class English(Language):
    consonants = list(consonants_en_2l_to_1l_arpabet.keys())
    vowels = [
        "AA",
        "AE",
        "AH",
        "AO",
        "AW",
        "AX",
        "AY",
        "EH",
        "EY",
        "IH",
        "IY",
        "OW",
        "OY",
        "UH",
        "UW",
        "AAR",
        "AOR",
        "AXR",
        "ER",
        "EYR",
        "IHR",
        "UHR",
        "EL",
        "EN",
        "EM",
    ]
    onsets = [
        ["P",],
        ['T',],
        ['K',],
        ['B',],
        ['D',],
        ['G',],
        ['F',],
        ['V',],
        ['TH',],
        ['DH',],
        ['S',],
        ['Z',],
        ['SH',],
        ['CH',],
        ['JH',],
        ['M',],
        ['N',],
        ['R',],
        ['L',],
        ['HH',],
        ['W',],
        ['Y',],
        ['P', 'R'],
        ['T', 'R'],
        ['K', 'R'],
        ['B', 'R'],
        ['D', 'R'],
        ['G', 'R'],
        ['F', 'R'],
        ['TH', 'R'],
        ['SH', 'R]'],
        ['P', 'L'],
        ['K', 'L'],
        ['B', 'L'],
        ['G', 'L'],
        ['F', 'L'],
        ['S', 'L'],
        ['T', 'W'],
        ['K', 'W'],
        ['D', 'W'],
        ['S', 'W'],
        ['S', 'P'],
        ['S', 'T'],
        ['S', 'K'],
        ['S', 'F'],
        ['S', 'M'],
        ['S', 'N'],
        ['G', 'W'],
        ['SH', 'W'],
        ['S', 'P', 'R'],
        ['S', 'P', 'L'],
        ['S', 'T', 'R'],
        ['S', 'K', 'R'],
        ['S', 'K', 'W'],
        ['S', 'K', 'L'],
        ['TH', 'W'],
        ['ZH',],
        ['P', 'Y'],
        ['K', 'Y'],
        ['B', 'Y'],
        ['F', 'Y'],
        ['HH', 'Y'],
        ['V', 'Y'],
        ['TH', 'Y'],
        ['M', 'Y'],
        ['S', 'P', 'Y'],
        ['S', 'K', 'Y'],
        ['G', 'Y'],
        ['HH', 'W'],
    ]


class MainProcessor:

    def process_args(self):
        parser = argparse.ArgumentParser()
        parser.add_argument(
          "source_file",
          help="input text",
          nargs="+",
        )
        parser.add_argument(
          "-o",
          dest="output_dir",
          help="output directory (default: same directory as input)"
        )
        parser.add_argument(
          "--dict",
          dest="dictionary_file_name",
          default="cmudict-0.7b.dict",
          help=("where the pronunciation dictionary to use is found (default: %(default)s).")
        )
        parser.add_argument(
          "--auxdict",
          dest="auxiliary_dictionaries",
          nargs="+",
          default=["cmu-addendum.dict"],
          help=("location of the auxiliary dictionary, used as a fallback for "
                "words the CMU does not know about. This option may be passed "
                "multiple times. Default: cmu-addendum.dict")
        )
        parser.add_argument(
          "--lyric-notation",
          dest="lyric_notation_file",
          default="lyric-notation.ily",
          help=("LilyPond file to include (default: lyric-notation.ily from the "
                "current directory"),
        )
        self.args = parser.parse_args()
        self.source_file_list = []
        for pattern in self.args.source_file:
            self.source_file_list.extend(glob.glob(pattern))

    def get_output_file_name(self, filename):
        if self.args.output_dir is None:
            output_dir = os.path.dirname(filename)
        else:
            output_dir = self.args.output_dir
        name, ext = os.path.splitext(filename)
        name = os.path.basename(name)
        lyric_notation = self.args.lyric_notation_file
        lyric_notation = os.path.relpath(lyric_notation, start=output_dir)
        return os.path.join(output_dir, name + ".ly"), lyric_notation



    def init_dict(self):
        self.dictionary = {}
        for filename in [self.args.dictionary_file_name] + self.args.auxiliary_dictionaries:
            try:
                with open(filename, "r") as file:
                    logging.info(f"Reading {filename}")
                    for line in file:
                        word, *syllables = line.split()
                        self.dictionary.setdefault(word, syllables)
            except FileNotFoundError:
                logging.error(f"File {filename} not found.")
                exit()

    def __init__(self):
        # Configurability could be added.
        self.language = English()
        self.process_args()
        self.init_dict()
        self.unfound_words = False

    # TODO: ugly!
    start = r"""
\version "2.22.0"
\include "%s"

songLyrics =
    \lyricmode {
    \goodColors
    \rgbVowelRainbow
    %s}

songSyllables =
    \lyricmode {
        \bracketSettings
        \bracketTextBeforeBreak
        \generalGrooveSettings
        \grooveTupletTextBeforeBreak
        \partial 4

"""

    end = """}
    \lyricNotation \songLyrics \songSyllables
        \pageBreak
    \oneLineLyricNotation \songLyrics \songSyllables
"""

    input_syllable_re = r"([A-Z']+)([.,;:?!]*)([0-9]*\.?)"


    def process_word(self):
        full_word = "".join(m.group(1) for m in self.current_word)
        try:
            phonemes = self.dictionary[full_word]
        except KeyError:
            logging.warning(f'Unknown word "{full_word.lower()}".')
            self.unfound_words = True
            print(f"[{full_word}]", end=" ", file=self.output_file)

        else:
            # Ohriner's addendum contains dashes to separate syllables. We
            # ignore them.
            phonemes = [phoneme for phoneme in phonemes if phoneme != "-"]
            syllables = self.language.syllabify(phonemes)
            word_len = len(self.current_word)
            syllables_len = len(syllables)
            if word_len != syllables_len:
                logging.warning(f"For word {full_word}, {word_len} syllables in input "
                                f"but {syllables_len} in dictionary. Did you forget "
                                f"double hyphens?")
            for m, syllable in zip(self.current_word, syllables):
                stress_level, onset, nucleus, coda = syllable
                stress = "`" if stress_level > 0 and len(self.current_word) > 1 else ""
                onset = [consonants_en_2l_to_1l_arpabet[consonant] for consonant in onset]
                if not onset:
                    onset = "Q"
                coda = [consonants_en_2l_to_1l_arpabet[consonant] for consonant in coda]
                nucleus = nucleus.lower()
                formatted_syllable = stress
                formatted_syllable += "".join(onset)
                formatted_syllable += "-" + nucleus
                if coda:
                    formatted_syllable += "-" + "".join(coda)
                punctuation = m.group(2)
                duration = m.group(3)
                formatted_syllable += duration
                # if punctuation:
                #     formatted_syllable += r" '"
                print(formatted_syllable, end=" ", file=self.output_file)


    def main(self):
        for filename in self.source_file_list:
            with open(filename) as file:
                input_text = file.read()
            self.current_word = []
            continue_current_word = True
            output_file_name, lyric_notation = self.get_output_file_name(filename)
            with open(output_file_name, "w") as file:
                self.output_file = file
                print(self.start % (lyric_notation, input_text), end="", file=file)
                for syllable in input_text.split():
                    # All processing is done with uppercase; that's what is
                    # found in the CMU dictionary.
                    syllable = syllable.upper()
                    match = re.fullmatch(self.input_syllable_re, syllable)
                    if continue_current_word:
                        # Coming from a "--", append syllable to the word
                        # being read.
                        if not match:
                            logging.warning(f"Not a valid syllable: {syllable}; ignoring")
                        else:
                            self.current_word.append(match)
                        # If the next syllable is not "--", the word should
                        # be terminated.
                        continue_current_word = False
                    elif syllable == "--":
                        # Mark the next syllable for appending to the word.
                        continue_current_word = True
                    elif match:
                        # Done reading a word. Process it, and reset it to
                        # the sole syllable that we've just found.
                        self.process_word()
                        self.current_word = [match]
                    else:
                        # May be bar check or other.
                        self.process_word()
                        self.current_word = []
                        continue_current_word = True
                        print(syllable, end=("\n" if syllable=="|" else " "), file=file)
                # For the last word, the processing must be triggered also.
                if self.current_word:
                    self.process_word()
                print(self.end, file=file)
            if self.unfound_words:
                logging.warning("Some words were not found. Placeholders have been "
                                "inserted. Please update auxiliary dictionary and re-run.")
            logging.info(f"Output written to {output_file_name}")


if __name__ == "__main__":
    MainProcessor().main()
