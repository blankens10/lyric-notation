\version "2.22.0"

#(define (recompute-music-length music)
  (let ((length-callback (ly:music-property music 'length-callback)))
    (if (procedure? length-callback)
        (set! (ly:music-property music 'length)
              (length-callback music))))
  music)

#(define (music-filter-map function music)
   "A variant of music-map that discards non-music results."
  (if (ly:music? music)
      (let ((es (ly:music-property music 'elements))
            (e (ly:music-property music 'element)))
        (if (pair? es)
            (set! (ly:music-property music 'elements)
                  (filter ly:music?
                          (map (lambda (y)
                                 (music-filter-map function y))
                               es))))
        (if (ly:music? e)
            (set! (ly:music-property music 'element)
                  (or (music-filter-map function e)
                      '())))
        (let ((result (function music)))
          (if (ly:music? result)
              (recompute-music-length result)
              #f)))
      #f))