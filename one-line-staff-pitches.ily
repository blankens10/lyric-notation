\version "2.22.0"

#(define (Fixup_staff_positions_when_one_line_engraver context)
   (make-engraver
     (acknowledgers
       ((note-head-interface engraver grob source-engraver)
          (let* ((layout (ly:grob-layout grob))
                 (setting (ly:output-def-lookup layout 'one-line-staff)))
            (if setting
                (ly:grob-set-property! grob 'staff-position 0)))))))

\layout {
  \context {
    \Score
    \consists #Fixup_staff_positions_when_one_line_engraver
  }
}