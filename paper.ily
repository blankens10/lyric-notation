\version "2.21.80"

#(ly:font-config-add-directory "./CharisSIL-5.000")

#(set! paper-alist
   (cons '("custom" . (cons (* 8.5 in) (* 11 in))) paper-alist))

\paper {
  #(set-paper-size "custom")
  
  % TODO: top-system-spacing, etc. must be adjusted
  left-margin = 1.25\in
  right-margin = 1.25\in
  top-margin = 1.25\in
  bottom-margin = 1.25\in
  
  % TODO: find way to adjust this automatically
  
  indent = 90
  
  system-system-spacing.basic-distance = 15
  #(define fonts
     (set-global-fonts
       #:roman "Charis SIL"
       #:factor (/ staff-height pt 20)
     ))
}