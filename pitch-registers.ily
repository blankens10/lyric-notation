\version "2.22.0"

hi =
#(make-music 'TextScriptEvent
             'direction UP
             ; Unicode logical AND.
             'text "∧")

lo =
#(make-music 'TextScriptEvent
             'direction UP
             ; Unicode logical OR.
             'text "∨")