\version "2.22.0"

% Useful macros for dealing with music properties and gathering
% grobs in engravers.

#(define-macro (prepend! thing obj)
   `(set! ,obj (cons ,thing ,obj)))

#(define-macro (prepend-elements! elts obj)
   `(set! ,obj (append ,elts ,obj)))