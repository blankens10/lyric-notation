\version "2.22.0"

\include "headphones.ily"

% Draw note heads with headphones and optional rhotacization.

#(use-modules (srfi srfi-26))

#(set-object-property! 'onset-classes 'backend-type? list?)
#(set-object-property! 'coda-classes 'backend-type? list?)
#(set-object-property! 'is-rhotacized 'backend-type? boolean?)
#(set-object-property! 'is-velarized 'backend-type? boolean?)
#(set-object-property! 'is-nasalized 'backend-type? boolean?)
#(set-object-property! 'is-masalized 'backend-type? boolean?)
#(set-object-property! 'is-gasalized 'backend-type? boolean?)
#(set-object-property! 'is-alt 'backend-type? boolean?)
#(set-object-property! 'hook-markup 'backend-type? markup?)
#(set-object-property! 'darkL-markup 'backend-type? markup?)
#(set-object-property! 'nasal-markup 'backend-type? markup?)
#(set-object-property! 'masal-markup 'backend-type? markup?)
#(set-object-property! 'gasal-markup 'backend-type? markup?)


#(define DEFAULT-HEADPHONE-COLORS
   '((glottal-stop (102 51 153))
     (plosive (255 51 51))
     (affricate (255 51 51) (255 255 0)) ; makes up two headphones
     (fricative (255 255 0))
     (nasal (0 255 0))
     (liquid (0 153 255))
     (glide (0 0 255))))

\layout {
  headphone-colors = #DEFAULT-HEADPHONE-COLORS
}

#(define (make-color lst)
   (apply rgb-color
          (map (cute / <> 255)
            lst)))


#(define (get-class-colors grob class)
   (or
     (assq-ref
       (ly:output-def-lookup
         (ly:grob-layout grob)
         'headphone-colors)
       class)
     (begin
       (ly:warning "No colors for category ~s"
                   class)
       '((0 0 0)))))

#(define (is-pointy-headphone-class? extended-class)
   (let ((class (first extended-class))
         (voiced (second extended-class)))
     (and (not voiced)
          (memq class
                '(affricate fricative plosive)))))

#(define (is-squared-headphone-class? extended-class)
   (let ((class (first extended-class)))
     (eq? class 'glottal-stop)))


#(define (make-headphone grob extended-class color)
   ((cond
      ((is-pointy-headphone-class? extended-class)
        pointy-headphone)
      ((is-squared-headphone-class? extended-class)
       glottal-headphone)
      (else
       regular-headphone))
    grob
    (make-color color)))

#(define (prepare-headphones grob extended-classes)
   ; colors-list-list here is a list of lists of colors:
   ; when an element of colors is a list of more than one
   ; element, several headphones are created with the
   ; same category (taken from the extended class) and
   ; different colors.
   (let* ((classes (map first extended-classes))
          (color-list-list (map (cute get-class-colors grob <>)
                                classes)))
     (map make-stencil-markup
          (apply append
                 (map
                   (lambda (extended-class color-list)
                     (make-headphone grob extended-class (first color-list))
                     (map (cute make-headphone grob extended-class <>)
                          color-list))
                   extended-classes
                   color-list-list)))))

#(define (calc-note-head-stencil grob)
   (let* ((onset-extended-classes (ly:grob-property grob 'onset-classes))
          (coda-extended-classes (ly:grob-property grob 'coda-classes))
          (left-headphones (prepare-headphones grob onset-extended-classes))
          (right-headphones (prepare-headphones grob coda-extended-classes))
          (is-rhotacized (ly:grob-property grob 'is-rhotacized))
          (is-velarized (ly:grob-property grob 'is-velarized))
          (is-nasalized (ly:grob-property grob 'is-nasalized))
          (is-masalized (ly:grob-property grob 'is-masalized))
          (is-gasalized (ly:grob-property grob 'is-gasalized))
          (is-alt (ly:grob-property grob 'is-alt))
          (note-head
            (if is-alt
                (diamond-note-head grob)
                (note-head-expanded grob)))
          (liquids
            (cond
              (is-rhotacized
                (ly:grob-property grob 'hook-markup))
              (is-velarized
                (ly:grob-property grob 'darkL-markup))
              (is-nasalized
                (ly:grob-property grob 'nasal-markup))
              (is-masalized
                (ly:grob-property grob 'masal-markup))
              (is-gasalized
                (ly:grob-property grob 'gasal-markup))
              (else empty-markup)))
          (staff-symbol (ly:grob-object grob 'staff-symbol))
          (factor (- (ly:staff-symbol-staff-space staff-symbol)
                     (* 2 (ly:staff-symbol-line-thickness staff-symbol)))))
     (ly:stencil-scale
       (stack-headphones grob
         (make-combine-markup (make-stencil-markup note-head)
                              liquids)
         left-headphones
         right-headphones)
       factor
       factor)))

headphonesOff = {
  \temporary \override Voice.NoteHead.onset-classes = #'()
  \temporary \override Voice.NoteHead.coda-classes = #'()
}

headphonesOn = {
  \revert Voice.NoteHead.onset-classes
  \revert Voice.NoteHead.coda-classes
}

\layout {
  \context {
    \Voice
    \override NoteHead.stencil = #calc-note-head-stencil
    \override NoteHead.onset-classes = #(grob::calc-property-by-copy 'onset-classes)
    \override NoteHead.coda-classes = #(grob::calc-property-by-copy 'coda-classes)
    \override NoteHead.is-rhotacized = #(grob::calc-property-by-copy 'is-rhotacized)
    \override NoteHead.is-velarized = #(grob::calc-property-by-copy 'is-velarized)
    \override NoteHead.is-nasalized = #(grob::calc-property-by-copy 'is-nasalized)
    \override NoteHead.is-masalized = #(grob::calc-property-by-copy 'is-masalized)
    \override NoteHead.is-gasalized = #(grob::calc-property-by-copy 'is-gasalized)
    \override NoteHead.is-alt = #(grob::calc-property-by-copy 'is-alt)
    \override Stem.layer = -1
    \override NoteHead.hook-markup =
      \markup
        \center-align
          \vcenter
            %%% \with-color #green \box
            \with-color #white
              % Toggle \with-color #green \box above to adjust extents.
              \fontsize #-6
              "ɚ"

    \override NoteHead.darkL-markup =
      \markup
        \center-align
          \vcenter
            %%% \with-color #green \box
            \with-color #white
              % Toggle \with-color #green \box above to adjust extents.
              \fontsize #-6
              "ɫ"
    \override NoteHead.nasal-markup =
      \markup
        \center-align
          \vcenter
            %%% \with-color #green \box
            \with-color #white
              % Toggle \with-color #green \box above to adjust extents.
              \fontsize #-6
              "n"
    \override NoteHead.masal-markup =
      \markup
        \center-align
          \vcenter
            %%% \with-color #green \box
            \with-color #white
              % Toggle \with-color #green \box above to adjust extents.
              \fontsize #-6
              "m"
    \override NoteHead.gasal-markup =
      \markup
        \center-align
          \vcenter
            %%% \with-color #green \box
            \with-color #white
              % Toggle \with-color #green \box above to adjust extents.
              \fontsize #-6
              "ŋ"

  }
  \context {
    \Score
    \override Stem.layer = -1
    \override StaffSymbol.layer = -2
  }
}
