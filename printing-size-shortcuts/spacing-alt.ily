\version "2.22.0"

% Quick way to set paper and margin sizes

%{
letter => 8.5 x 11 - 1.25 in margins
wide => 17 x 11 - 1.25 in margins
tall => 11 x 17 - 1.25 in margins
justP => 6I  x 8.5 - 0.05 in margins
justL => 8.5 x 11 - 0.05 in margins
smallP => 8.5 x 11 - 0.05 in margins
smallP => 8.5 x 11 - 0.05 in margins

  #(set! paper-alist
   (cons '("letter" . (cons (* 8.5 in)
                            (* 11 in)))
   (cons '("letterL" . (cons (* 11 in)
                            (* 8.5 in)))
   (cons '("wide" . (cons (* 17 in)
                          (* 11 in)))
    (cons '("tall" . (cons (* 11 in)
                          (* 17 in)))
    (cons '("justP" . (cons (* 6 in)
                          (* 8.5  in)))
    (cons '("justL" . (cons (* 8.5 in)
                          (* 6  in)))
    (cons '("smallP" . (cons (* 4.25 in)
                            (* 6  in)))
    (cons '("smallL" . (cons (* 6 in)
                            (* 4.25 in)))
      paper-alist)))))))))



 %}

\paper {
  % For visualisation
  %% annotate-spacing = ##t

  #(set-paper-size "justP")
  #(set-global-staff-size 20)

  left-margin = 0.05\in
  right-margin = 0.05\in
  top-margin = 0.05\in
  bottom-margin = 0.05\in
  indent = 0

  top-system-spacing = \noDistance
  last-bottom-spacing = \noDistance

  system-system-spacing =
    #`((basic-distance . 24)
       (minimum-distance . 0)
       (padding . 2)
       (stretchability . 500))

  #(define fonts
     (set-global-fonts
       #:roman "Charis SIL"
       #:factor (/ staff-height pt 20)))

  oddHeaderMarkup = #empty-markup
  evenHeaderMarkup = #empty-markup
  oddFooterMarkup = #empty-markup
  evenFooterMarkup = #empty-markup
  ragged-right = ##f
  ragged-bottom = ##t
}
