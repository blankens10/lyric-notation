\version "2.22.0"

% Code inspired from text-engraver.cc
% ('direction is set in the context below.)
#(define (engraver-create-text-script engraver mkup cause)
   (let ((script (ly:engraver-make-grob engraver 'TextScript cause)))
     (ly:grob-set-property! script 'text mkup)
     (ly:grob-set-property! script 'direction UP)
     script))


#(define (Process_stress_accent_engraver context)
   (make-engraver
      (listeners
        ((stress-event engraver event)
           (engraver-create-text-script engraver
                                        (ly:context-property context 'stressMarkup)
                                        event))
        ((accent-event engraver event)
           (engraver-create-text-script engraver
                                        (ly:context-property context 'accentMarkup)
                                        event)))))


\layout {
  \context {
    \Voice
    \consists #Process_stress_accent_engraver
  }
}