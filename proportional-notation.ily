\version "2.22.0"

\layout {
  \context {
    \Score
    proportionalNotationDuration = #(ly:make-moment 1)
    \override SpacingSpanner.uniform-stretching = ##t
    \override SpacingSpanner.strict-note-spacing = ##t
  }
}