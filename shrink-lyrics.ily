\version "2.22.0"

shrinkLyrics =
#(define-music-function (shrink) (number?)
   #{ \override Lyrics.LyricText.font-size = #(- shrink) #})