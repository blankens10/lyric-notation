\version "2.22.0"

% All spacing values (and font selection).

#(ly:font-config-add-directory "./CharisSIL-5.000")

%{
  Turn on the big page with
   \paper { #(set-paper-size "big") }
%}

#(set! paper-alist
   (cons '("custom" . (cons (* 8.5 in)
                            (* 11 in)))
         (cons '("wide" . (cons (* 17 in)
                               (* 11 in)))
          (cons '("just" . (cons (* 6.25 in)
                                (* 8.75 in)))
               paper-alist))))

#(set-global-staff-size 19)

noDistance =
#'((basic-distance . 0)
   (minimum-distance . 0)
   (padding . 0)
   (stretchability . 0))

\paper {
  % For visualisation
  %% annotate-spacing = ##t

  #(set-paper-size "just")

  left-margin = 0.2\in
  right-margin = 0.2\in
  top-margin = 0.2\in
  bottom-margin = 0.2\in
  indent = 0

  top-system-spacing = \noDistance
  last-bottom-spacing = \noDistance

  system-system-spacing =
    #`((basic-distance . 24)
       (minimum-distance . 0)
       (padding . 2)
       (stretchability . 500))

  #(define fonts
     (set-global-fonts
       #:roman "Charis SIL"
       #:factor (/ staff-height pt 20)))

  oddHeaderMarkup = #empty-markup
  evenHeaderMarkup = #empty-markup
  oddFooterMarkup = #empty-markup
  evenFooterMarkup = #empty-markup
  ragged-right = ##f
  ragged-bottom = ##t
}
