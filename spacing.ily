\version "2.22.0"

% All spacing values (and font selection).

#(ly:font-config-add-directory "./CharisSIL-5.000")

%{
  Turn on the big page with
   \paper { #(set-paper-size "big") }
%}

#(set! paper-alist
   (cons '("letter" . (cons (* 8.5 in)
                            (* 11 in)))
   (cons '("letterL" . (cons (* 11 in)
                            (* 8.5 in)))
    (cons '("tall" . (cons (* 11 in)
                          (* 17 in)))
    (cons '("wide" . (cons (* 17 in)
                          (* 11 in)))
    (cons '("justP" . (cons (* 6 in)
                          (* 8.5  in)))
    (cons '("justL" . (cons (* 8.5 in)
                          (* 6  in)))
    (cons '("smallP" . (cons (* 4.25 in)
                            (* 6  in)))
    (cons '("smallL" . (cons (* 6 in)
                            (* 4.25 in)))
      paper-alist)))))))))


#(set-global-staff-size 20)

noDistance =
#'((basic-distance . 0)
   (minimum-distance . 0)
   (padding . 0)
   (stretchability . 0))

\paper {
  % For visualisation
  %% annotate-spacing = ##t

  #(set-paper-size "letter")

  left-margin = 1.25\in
  right-margin = 1.25\in
  top-margin = 1.25\in
  bottom-margin = 1.25\in
  indent = 0

  top-system-spacing = \noDistance
  last-bottom-spacing = \noDistance

  system-system-spacing =
    #`((basic-distance . 24)
       (minimum-distance . 0)
       (padding . 2)
       (stretchability . 500))

  #(define fonts
     (set-global-fonts
       #:roman "Charis SIL"
       #:factor (/ staff-height pt 20)))

  oddHeaderMarkup = #empty-markup
  evenHeaderMarkup = #empty-markup
  oddFooterMarkup = #empty-markup
  evenFooterMarkup = #empty-markup
  ragged-right = ##f
  ragged-bottom = ##t
}

letterP =
\paper {
  #(set-paper-size "letter")
  left-margin = 1.25\in
  right-margin = 1.25\in
  top-margin = 1.25\in
  bottom-margin = 1.25\in
  indent = 0

  top-system-spacing = \noDistance
  last-bottom-spacing = \noDistance

  system-system-spacing =
    #`((basic-distance . 24)
       (minimum-distance . 0)
       (padding . 2)
       (stretchability . 500))

  #(define fonts
     (set-global-fonts
       #:roman "Charis SIL"
       #:factor (/ staff-height pt 20)))

  oddHeaderMarkup = #empty-markup
  evenHeaderMarkup = #empty-markup
  oddFooterMarkup = #empty-markup
  evenFooterMarkup = #empty-markup
  ragged-right = ##f
  ragged-bottom = ##t
}

tall =
\paper {
  #(set-paper-size "tall")
  left-margin = 1.25\in
  right-margin = 1.25\in
  top-margin = 1.25\in
  bottom-margin = 1.25\in
  indent = 0

  top-system-spacing = \noDistance
  last-bottom-spacing = \noDistance

  system-system-spacing =
    #`((basic-distance . 24)
       (minimum-distance . 0)
       (padding . 2)
       (stretchability . 500))

  #(define fonts
     (set-global-fonts
       #:roman "Charis SIL"
       #:factor (/ staff-height pt 20)))

  oddHeaderMarkup = #empty-markup
  evenHeaderMarkup = #empty-markup
  oddFooterMarkup = #empty-markup
  evenFooterMarkup = #empty-markup
  ragged-right = ##f
  ragged-bottom = ##t
}

wide =
\paper {
  #(set-paper-size "wide")
  left-margin = 1.25\in
  right-margin = 1.25\in
  top-margin = 1.25\in
  bottom-margin = 1.25\in
  indent = 0

  top-system-spacing = \noDistance
  last-bottom-spacing = \noDistance

  system-system-spacing =
    #`((basic-distance . 24)
       (minimum-distance . 0)
       (padding . 2)
       (stretchability . 500))

  #(define fonts
     (set-global-fonts
       #:roman "Charis SIL"
       #:factor (/ staff-height pt 20)))

  oddHeaderMarkup = #empty-markup
  evenHeaderMarkup = #empty-markup
  oddFooterMarkup = #empty-markup
  evenFooterMarkup = #empty-markup
  ragged-right = ##f
  ragged-bottom = ##t
}

justP =
  %{ #(set-global-staff-size 15) %}
  \paper {
    % For visualisation
    %% annotate-spacing = ##t

    #(set-paper-size "justP")

    left-margin = 0.1\in
    right-margin = 0.05\in
    top-margin = 0.05\in
    bottom-margin = 0.05\in
    indent = 0

    top-system-spacing = \noDistance
    last-bottom-spacing = \noDistance

    system-system-spacing =
      #`((basic-distance . 24)
         (minimum-distance . 0)
         (padding . 2)
         (stretchability . 500))

    #(define fonts
       (set-global-fonts
         #:roman "Charis SIL"
         #:factor (/ staff-height pt 20)))

    oddHeaderMarkup = #empty-markup
    evenHeaderMarkup = #empty-markup
    oddFooterMarkup = #empty-markup
    evenFooterMarkup = #empty-markup
    ragged-right = ##f
    ragged-bottom = ##t
  }

smallP =
  \paper {
    % For visualisation
    %% annotate-spacing = ##t
    #(set-paper-size "smallP")

    left-margin = 0.05\in
    right-margin = 0.05\in
    top-margin = 0.05\in
    bottom-margin = 0.05\in
    indent = 0

    top-system-spacing = \noDistance
    last-bottom-spacing = \noDistance

    system-system-spacing =
      #`((basic-distance . 24)
         (minimum-distance . 0)
         (padding . 2)
         (stretchability . 500))

    #(define fonts
       (set-global-fonts
         #:roman "Charis SIL"
         #:factor (/ staff-height pt 20)))

    oddHeaderMarkup = #empty-markup
    evenHeaderMarkup = #empty-markup
    oddFooterMarkup = #empty-markup
    evenFooterMarkup = #empty-markup
    ragged-right = ##f
    ragged-bottom = ##t
}
