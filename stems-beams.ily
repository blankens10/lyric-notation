\version "2.22.0"

\include "switch-one-line.ily"

%{
  Set beam and stem lengths. The goals are:

  - All beams and stem stick out from the staff.
  - All stems have the same length.
  - All beams have the same distance to the staff.
    This means that beams with many stemlets (e.g.,
    for 32th notes) have their top higher than those
    with few stemlets.
%}


#(set-object-property! 'base-position 'backend-type? number?)
#(set-object-property! 'end-position 'backend-type? number?)


% Simply copied from functions found in lily/beam.cc

#(define (max-beam-count grob)
   (define (min-beams left right)
     (cond ((and left right)
            (min (apply min left)
                 (apply min right)))
           (left
            (apply min left))
           (right
            (apply min right))
           (else
            #f)))
   (let* ((beaming
            ; No idea why (ly:grob-property grob 'beaming) doesn't
            ; work here.
            (map (lambda (stem)
                   (ly:grob-property stem 'beaming))
                 (ly:grob-array->list
                   (ly:grob-object grob 'stems))))
          (min-beam (apply min
                           (cons 0
                                 (filter-map min-beams
                                             (map car beaming)
                                             (map cdr beaming)))))
          (max-beam-count (1+ (- min-beam))))
     max-beam-count))
          
          
#(define (get-beam-translation grob)
   (let* ((beam-count (max-beam-count grob))
          (staff-symbol (ly:grob-object grob 'staff-symbol))
          (staff-space (ly:staff-symbol-staff-space staff-symbol))
          (line-thickness (ly:staff-symbol-line-thickness staff-symbol))
          (beam-thickness (* (ly:grob-property grob 'beam-thickness)
                             staff-space))
          (fraction (ly:grob-property grob 'length-fraction 1.0)))
     (+ (* staff-space fraction)
        (/ (- (* line-thickness fraction)
              beam-thickness)
           (if (< beam-count 4)
               2.0
               3.0)))))

#(define (calc-beam-positions grob)
   (let* ((base (ly:grob-property grob 'base-position))
          (translation (get-beam-translation grob))
          (beam-count (max-beam-count grob))
          (beam-thickness (ly:grob-property grob 'beam-thickness))
          (position (+ base
                       (* translation
                          (1- beam-count))
                       beam-thickness)))
     (cons position position)))


#(define (calc-stem-length grob)
   (- (ly:grob-property grob 'end-position)
      (ly:grob-property grob 'stem-begin-position)))


\layout {
  \context {
    \Voice
    \override Beam.base-position = #(switch-based-on-one-line 6.3 3.1)
    \override Beam.positions = #calc-beam-positions
    \override Stem.end-position = #(switch-based-on-one-line 18 10)
    \override Stem.length = #calc-stem-length
  }
}