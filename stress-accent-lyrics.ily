\version "2.22.0"

#(define (Stress_accent_lyrics_engraver context)
   (let ((stress-made #f)
         (accent-made #f))
     (make-engraver
       (listeners
         ((stress-event engraver event)
            (set! stress-made #t))
         ((accent-event engraver event)
            (set! accent-made #t)))
       (acknowledgers
         ((lyric-syllable-interface engraver grob source-engraver)
            ; In the case of rests, we don't have any
            ; LyricText grob.
            (if grob
                (begin
                  (if (or stress-made accent-made)
                      (ly:grob-set-property! grob 'font-series 'bold))
                  (if accent-made
                      (ly:grob-set-property! grob 'font-shape 'italic))))))
       ((stop-translation-timestep engraver)
          (set! stress-made #f)
          (set! accent-made #f)))))


\layout {
  \context {
    \Score
    \consists #Stress_accent_lyrics_engraver
  }
}