\version "2.22.0"

#(define ((switch-based-on-one-line normal-value one-line-value) grob)
   (if (ly:output-def-lookup
         (ly:grob-layout grob)
         'one-line-staff)
       one-line-value
       normal-value))